use bevy::{
    app::AppExit,
    asset::{AssetLoader, LoadContext, LoadedAsset},
    input::{keyboard::KeyboardInput, ButtonState},
    prelude::*,
    reflect::TypeUuid,
    render::render_resource::{Extent3d, TextureDimension, TextureFormat},
    utils::BoxedFuture,
};
use bevy_egui::{egui, EguiPlugin, EguiContexts};
use bevy_pancam::{PanCam, PanCamPlugin};
use rand::prelude::*;

use data_mandel::{BuddhabrotConfig, InterestingPoint, MandelData, MandelState};

#[derive(Debug, Default, Resource)]
struct LastRenderTime(f32);

#[derive(Resource)]
struct BuddhabrotConfigResource(BuddhabrotConfig);

#[derive(Component)]
struct MandelDataComponent(MandelData);

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(EguiPlugin)
        .add_plugin(PanCamPlugin::default())
        .add_asset::<Points>()
        .init_asset_loader::<Points>()
        .insert_resource(BuddhabrotConfigResource(BuddhabrotConfig {
            cache_source: data_mandel::CacheSource::GenGrid { jitter: true },
            // cache_source: data_mandel::CacheSource::DataLoad,
            paint_style: data_mandel::PaintStyle::CachedMerge,
            blank_style: data_mandel::BlankStyle::GreenToBlueFade,
            limit: 100_000_000,
        }))
        .init_resource::<LastRenderTime>()
        .init_resource::<RenderSettings>()
        .add_startup_system(init_buddhabrot)
        .add_system(main_interface)
        .add_system(input_handler)
        .add_system(render_buddhabrot)
        .run();
}

#[derive(Default, TypeUuid)]
#[uuid = "d58c4629-42dc-4660-9a76-2d3a20ed6965"]
struct Points {
    order: u8,
    data: Vec<InterestingPoint>,
}

impl AssetLoader for Points {
    fn load<'a>(
        &'a self,
        bytes: &'a [u8],
        load_context: &'a mut LoadContext,
    ) -> BoxedFuture<'a, Result<(), anyhow::Error>> {
        Box::pin(async move {
            info!("Parsing interesting point file");
            let points = String::from_utf8(bytes.to_owned())?
                .lines()
                .map(serde_json::from_str)
                .collect::<Result<_, serde_json::Error>>()?;

            let order = data_mandel::order_from_filename(load_context.path())
                .expect("couldn't parse filename");

            load_context.set_default_asset(LoadedAsset::new(Self {
                order,
                data: points,
            }));

            Ok(())
        })
    }

    fn extensions(&self) -> &[&str] {
        &["ndjson"]
    }
}

#[derive(Resource)]
struct BuddhabrotTexture(Handle<Image>);

#[derive(Debug, Default, Resource)]
struct PointsHandles(Vec<Handle<Points>>);

fn init_buddhabrot(
    mut commands: Commands,
    asset_server: ResMut<AssetServer>,
    mut textures: ResMut<Assets<Image>>,
) {
    let points = asset_server
        .load_folder("data")
        .expect("Couldn't start loading data points?")
        .into_iter()
        .map(HandleUntyped::typed)
        .collect();

    commands.insert_resource(PointsHandles(points));

    let texture_width = 2048;

    let texture_handle = textures.add(Image::new_fill(
        Extent3d {
            width: texture_width,
            height: texture_width,
            depth_or_array_layers: 1,
        },
        TextureDimension::D2,
        &[0, 0, 0, 255],
        TextureFormat::Rgba8Unorm,
    ));
    commands.insert_resource(BuddhabrotTexture(texture_handle.clone()));

    commands.spawn(Camera2dBundle::default())
        .insert(PanCam::default());

    commands.spawn(SpriteBundle {
        texture: texture_handle,
        ..SpriteBundle::default()
    });
}

#[derive(Debug, Default)]
struct WindowSetup {
    resolution_set: bool,
}

#[derive(Debug, Default, Resource)]
struct RenderSettings {
    paused: bool,
}

#[allow(clippy::too_many_arguments)]
fn render_buddhabrot(
    texture_handle: Res<BuddhabrotTexture>,
    mut textures: ResMut<Assets<Image>>,
    buddhabrot_config: Res<BuddhabrotConfigResource>,
    time: Res<Time>,
    mut windows: Query<&mut Window>,
    mut window_setup: Local<WindowSetup>,
    data: Query<&MandelDataComponent>,
    mut last_render_time: ResMut<LastRenderTime>,
    render_settings: Res<RenderSettings>,
) {
    if render_settings.paused {
        return;
    }

    let start = std::time::Instant::now();
    if !window_setup.resolution_set {
        let mut window = windows.single_mut();
        window.resolution.set(1374.0, 1100.0);
        window.title = "Wraithan's Shader Playground".to_string();
        window_setup.resolution_set = true;
        info!("Set window resolution");
    }

    let texture = textures.get_mut(&texture_handle.0).expect("should exist");
    let mut current_time = time.elapsed_seconds();

    if let data_mandel::PaintStyle::CachedMerge = buddhabrot_config.0.paint_style {
        current_time = ((current_time % 16.0) - 8.0).abs() / 4.0;
    }

    let row_length = texture.size().x as usize;
    let texture_ref: &mut [u8] = &mut texture.data;
    let chunked_ref: &mut [[u8; 4]] = bytemuck::cast_slice_mut(texture_ref);
    buddhabrot_config.0.blank_style.blank_texture(chunked_ref);
    buddhabrot_config.0.paint_style.paint_texture(
        data.iter().map(|v| &v.0).collect::<Vec<&MandelData>>(),
        chunked_ref,
        row_length,
        current_time,
        buddhabrot_config.0.limit,
    );
    last_render_time.0 = start.elapsed().as_secs_f32();
}

fn main_interface(
    mut commands: Commands,
    mut egui_contexts: EguiContexts,
    point_assets: Res<Assets<Points>>,
    mut buddhabrot_config: ResMut<BuddhabrotConfigResource>,
    data: Query<(Entity, &MandelDataComponent)>,
    last_render_time: Res<LastRenderTime>,
    mut render_settings: ResMut<RenderSettings>,
) {
    egui::SidePanel::left("Top Level Side Panel")
        .default_width(350.0)
        .show(egui_contexts.ctx_mut(), |ui| {
            egui::CollapsingHeader::new("Render settings")
                .default_open(true)
                .show(ui, |ui| {
                    let mut selected_paint = buddhabrot_config.0.paint_style;
                    ui.horizontal(|ui| {
                        ui.label("Painting Style");
                        egui::ComboBox::from_id_source("Painting Style")
                            .selected_text(format!("{selected_paint:?}"))
                            .show_ui(ui, |ui| {
                                ui.set_min_width(150.0);
                                ui.selectable_value(
                                    &mut selected_paint,
                                    data_mandel::PaintStyle::CachedMerge,
                                    "CachedMerge",
                                );
                                // ui.selectable_value(
                                //     &mut selected_paint,
                                //     data_mandel::PaintStyle::Petalbrot,
                                //     "Petalbrot",
                                // );
                                ui.selectable_value(
                                    &mut selected_paint,
                                    data_mandel::PaintStyle::CachedDataOrigin,
                                    "CachedDataOrigin",
                                );
                                ui.selectable_value(
                                    &mut selected_paint,
                                    data_mandel::PaintStyle::Density,
                                    "Density",
                                );
                                ui.selectable_value(
                                    &mut selected_paint,
                                    data_mandel::PaintStyle::DensityScan,
                                    "DensityScan",
                                );
                                if selected_paint != buddhabrot_config.0.paint_style {
                                    buddhabrot_config.0.paint_style = selected_paint;
                                }
                            });
                    });

                    let mut selected_blank = buddhabrot_config.0.blank_style;
                    ui.horizontal(|ui| {
                        ui.label("Blanking Style");
                        egui::ComboBox::from_id_source("Blanking Style")
                            .selected_text(format!("{selected_blank:?}"))
                            .show_ui(ui, |ui| {
                                use data_mandel::BlankStyle::*;
                                ui.set_min_width(150.0);
                                ui.selectable_value(&mut selected_blank, Black, "Black");
                                ui.selectable_value(&mut selected_blank, CycleFade, "CycleFade");
                                ui.selectable_value(
                                    &mut selected_blank,
                                    GreenToBlueFade,
                                    "GreenToBlueFade",
                                );
                                ui.selectable_value(
                                    &mut selected_blank,
                                    GreenToRedFade,
                                    "GreenToRedFade",
                                );
                                ui.selectable_value(&mut selected_blank, NoFade, "NoFade");
                                if selected_blank != buddhabrot_config.0.blank_style {
                                    buddhabrot_config.0.blank_style = selected_blank;
                                }
                            });
                    });
                });
            egui::CollapsingHeader::new("Render info")
                .default_open(true)
                .show(ui, |ui| {
                    // let cache_stats = buddhabrot::get_cache_stats();
                    ui.label(format!("Interesting points: {}", data.iter().len()));
                    ui.label(format!("Last Data Render Time: {:.3}s", last_render_time.0));
                    ui.label(format!(
                        "Potential Renders Per Second: {:.1}",
                        1.0 / last_render_time.0
                    ));
                });

            egui::CollapsingHeader::new("Interesting Points DB")
                .default_open(true)
                .show(ui, |ui| {
                    let mut rng = &mut rand::thread_rng();
                    let mut sorted_points = point_assets.iter().map(|(_, p)| p).collect::<Vec<_>>();
                    sorted_points.sort_by_key(|p| p.order);
                    for points in sorted_points {
                        ui.horizontal(|ui| {
                            ui.label(format!("Order {}", points.order));

                            let mut compute = |num| {
                                points
                                    .data
                                    .choose_multiple(&mut rng, num)
                                    .map(InterestingPoint::compute_mandelbrot)
                                    .filter(|mandel_data| mandel_data.state == MandelState::Outside)
                                    .for_each(|data| {
                                        let mut flipped_point = InterestingPoint::from(&data);
                                        flipped_point.coordinates.1 *= -1.0;
                                        let flipped_data = flipped_point.compute_mandelbrot();
                                        commands.spawn(MandelDataComponent(data));
                                        commands.spawn(MandelDataComponent(flipped_data));
                                    })
                            };

                            if ui.button("1").clicked() {
                                compute(1);
                            }
                            if ui.button("10").clicked() {
                                compute(10);
                            }
                            if ui.button("100").clicked() {
                                compute(100);
                            }
                            if ui.button("1k").clicked() {
                                compute(1000);
                            }
                            if ui.button("10k").clicked() {
                                compute(10_000);
                            }
                            if ui.button("100k").clicked() {
                                compute(100_000);
                            }
                            if ui.button(format!("All ({})", points.data.len())).clicked() {
                                compute(points.data.len());
                            }
                        });
                    }
                    if ui.button("Clear render").clicked() {
                        for (entity, _) in data.iter() {
                            commands.entity(entity).despawn_recursive();
                        }
                    }
                    if ui.button("Pause Render").clicked() {
                        render_settings.paused = !render_settings.paused;
                    }
                });
        });
}

fn input_handler(
    mut commands: Commands,
    mut keyboard_input_events: EventReader<KeyboardInput>,
    mut app_exit_events: EventWriter<AppExit>,
    data: Query<(Entity, &MandelDataComponent)>,
    mut render_settings: ResMut<RenderSettings>,
) {
    for event in keyboard_input_events.iter() {
        if let Some(key_code) = event.key_code {
            if event.state == ButtonState::Pressed {
                match key_code {
                    KeyCode::Escape | KeyCode::Q => app_exit_events.send(AppExit),
                    KeyCode::C => {
                        for (entity, _) in data.iter() {
                            commands.entity(entity).despawn_recursive();
                        }
                    }
                    KeyCode::P => {
                        render_settings.paused = !render_settings.paused;
                    }
                    _ => (),
                }
            }
        }
    }
}
