use std::{io::Write, path::PathBuf, sync::Mutex};

use clap::Parser;
use data_mandel::{compute_mandelbrot, order_from_filename, InterestingPoint, MandelState};
use rayon::prelude::*;
use tracing::{info, warn};

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::TRACE)
        .with_level(true)
        .with_thread_names(true)
        .init();

    run_comparator();
}

/// Shader Playground Buddhabrot Data Comparator
///
/// Loads an order file and validates the iterations
#[derive(Debug, Parser)]
#[command(name = "comparator")]
pub struct DataCompConfig {
    /// File to validate
    data_file: PathBuf,
    /// overwrite iterations for points that stayed in the same order
    #[arg(short, long)]
    update_data: bool,
}

struct OutOfOrderPoint {
    coordinatates: (f64, f64),
    expected_iterations: usize,
    actual_iterations: usize,
    order: usize,
}

pub fn run_comparator() {
    let config = DataCompConfig::parse();
    let mut data_points = data_mandel::load_data_from_file(&config.data_file, false);
    let expected_order =
        order_from_filename(&config.data_file).expect("Couldn't parse filename?") as usize;
    let limit = 10usize.pow(expected_order as u32);
    let out_of_order: Mutex<Vec<OutOfOrderPoint>> = Mutex::default();
    let start = std::time::Instant::now();
    data_points.par_iter_mut().for_each(|mut point| {
        let (x, y) = point.coordinates;
        let computed_data = compute_mandelbrot(x, y, limit, Some(point.iterations));
        let actual_iterations = computed_data.data.len();
        let actual_order = data_mandel::get_order(actual_iterations);
        if let MandelState::Outside = computed_data.state {
            if point.iterations != actual_iterations {
                info!(actual_order, expected_order, actual_iterations, ?point);
                if config.update_data {
                    if expected_order == actual_order {
                        point.iterations = actual_iterations;
                    } else {
                        let mut ooo = out_of_order.lock().expect("failed to get ooo lock");
                        ooo.push(OutOfOrderPoint {
                            coordinatates: point.coordinates,
                            expected_iterations: point.iterations,
                            actual_iterations,
                            order: actual_order,
                        });
                    }
                }
            }
        } else {
            warn!(?point, "This point now inside of mandelbrot");
        }
    });
    info!(
        num_points = data_points.len(),
        duration = ?start.elapsed(),
        "Validated points"
    );
    if config.update_data {
        let ooo = out_of_order
            .into_inner()
            .expect("Couldn't take over ooo vec");
        let data_points: Vec<InterestingPoint> = data_points
            .into_iter()
            .filter(|point| {
                !ooo.iter()
                    .any(|ooo_point| ooo_point.expected_iterations == point.iterations)
            })
            .collect();
        {
            // Rewrite the order file we loaded since we updated values.
            let mut output_file = std::fs::OpenOptions::new()
                .write(true)
                .truncate(true)
                .open(&config.data_file)
                .expect("Couldn't open output file, does data folder exist?");
            for point in &data_points {
                output_file
                    .write_all(&serde_json::to_vec(&point).unwrap())
                    .unwrap();
                output_file.write_all(b"\n").unwrap();
            }
        }
        {
            let output_folder = config
                .data_file
                .parent()
                .expect("data file should have parent dir");
            for point in ooo {
                let file_name = output_folder.join(format!("order-{}.ndjson", point.order));
                let mut output_file = std::fs::OpenOptions::new()
                    .create(true)
                    .write(true)
                    .append(true)
                    .open(file_name)
                    .expect("Couldn't open output file, does data folder exist?");

                output_file
                    .write_all(
                        &serde_json::to_vec(&InterestingPoint {
                            coordinates: point.coordinatates,
                            iterations: point.actual_iterations,
                        })
                        .unwrap(),
                    )
                    .unwrap();
                output_file.write_all(b"\n").unwrap();
            }
        }
    }
}
