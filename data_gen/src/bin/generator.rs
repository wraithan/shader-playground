use std::{
    collections::HashMap,
    io::Write,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
    time::Duration,
};

use clap::Parser;
use crossbeam_channel::unbounded;
use crossbeam_utils::sync::WaitGroup;
use data_mandel::{compute_mandelbrot, InterestingPoint, MandelData, MandelState};
use rand::Rng;
use tracing::info;

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::TRACE)
        // .with_env_filter("data_mandel=trace")
        .with_level(true)
        .with_thread_names(true)
        .init();

    run_generation();
}

/// Shader Playground Buddhabrot Data Generator
///
/// Runs the mandelbrot algorithm on random points looking for ones that fit the bounds specified by the args.
#[derive(Debug, Parser)]
#[command(name = "generator")]
pub struct DataGenConfig {
    /// Max number of iterations before a point is considered infinite
    #[arg(index = 2)]
    upper_bound: usize,
    /// Min number of iterations before a point is considered interesting enough to keep
    #[arg(index = 1)]
    lower_bound: usize,
    /// Number of cores to dedicate to the search
    #[arg(short, long, num_args = 1, default_value = "8")]
    jobs: usize,
}

pub fn run_generation() {
    let config = DataGenConfig::parse();
    // let count = 1;
    let limit = config.upper_bound;
    let cutoff = config.lower_bound;
    let lowest_order = data_mandel::get_order(cutoff);
    let highest_order = data_mandel::get_order(limit);
    info!(cutoff, limit, lowest_order, highest_order, "Starting run!");

    let found = Arc::new(AtomicUsize::new(0));
    let checked = Arc::new(AtomicUsize::new(0));
    let (save_send, save_recv) = unbounded::<InterestingPoint>();
    let wg = WaitGroup::new();
    {
        let output: std::path::PathBuf = "data".into();

        let wg = wg.clone();
        std::thread::spawn(move || {
            let mut output_files = HashMap::new();
            for order in lowest_order..=highest_order {
                let file_name = output.join(format!("order-{order}.ndjson"));
                info!(?file_name, "Opening output files");
                let output_file = std::fs::OpenOptions::new()
                    .create(true)
                    .write(true)
                    .append(true)
                    .open(file_name)
                    .expect("Couldn't open output file, does data folder exist?");
                output_files.insert(order, output_file);
            }
            while let Ok(data) = save_recv.recv() {
                let order = data_mandel::get_order(data.iterations);
                let output_file = output_files
                    .get_mut(&order)
                    .expect("Couldn't find output file for order?");
                output_file
                    .write_all(&serde_json::to_vec(&data).unwrap())
                    .unwrap();
                output_file.write_all(b"\n").unwrap();
            }
            drop(wg);
        });
    }
    info!("Spinning up checked worker");
    {
        let checked = checked.clone();
        let mut last_n = [0; 30];
        let jobs = config.jobs;
        std::thread::spawn(move || loop {
            for val in &mut last_n {
                std::thread::sleep(Duration::from_micros((1000 * 1000) - 30));
                *val = checked.swap(0, Ordering::Relaxed);
            }
            let average = last_n.iter().sum::<usize>() / last_n.len();
            info!(
                average,
                per_thread = average / jobs,
                "Per second in the last {} seconds",
                last_n.len()
            );
        });
    }
    info!(worker_count = config.jobs, "Spinning up workers");
    for _ in 0..config.jobs {
        let save_send = save_send.clone();
        let found = found.clone();
        let checked = checked.clone();
        std::thread::spawn(move || {
            let mut rng = rand::thread_rng();
            loop {
                let x = (rng.gen::<f64>() * 4.0) - 2.0;
                let y = (rng.gen::<f64>() * 4.0) - 2.0;
                // dbg!((x, y));
                if let MandelData {
                    state: MandelState::Outside,
                    data,
                    coordinates,
                } = compute_mandelbrot(x, y, limit, None)
                {
                    if data.len() > cutoff {
                        {
                            let current_count = found.fetch_add(1, Ordering::Relaxed);
                            info!(
                                iterations = data.len(),
                                current_count,
                                order = data_mandel::get_order(data.len()),
                                "Found point"
                            );
                        }
                        let point = InterestingPoint {
                            coordinates,
                            iterations: data.len(),
                        };
                        save_send.send(point).unwrap();
                    }
                }
                checked.fetch_add(1, Ordering::Relaxed);
            }
        });
    }
    drop(save_send);
    wg.wait();
}
