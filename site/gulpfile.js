const { src, dest, parallel, watch: watchGulp } = require('gulp')
const less = require('gulp-less')
const minifyCSS = require('gulp-csso')
const debug = require('gulp-debug')
const gulpClean = require('gulp-clean')

function css () {
  return src('src/less/**/*.less', { sourcemaps: true })
    .pipe(debug({ title: 'less:' }))
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(dest('build/css'), { sourcemaps: true })
    .pipe(debug({ title: 'css:' }))
}

function js () {
  return src('src/js/**/*.js', { sourcemaps: true })
    .pipe(debug({ title: 'js:' }))
    .pipe(dest('build/js'), { sourcemaps: true })
}

function html () {
  return src('src/**/*.html', { sourcemaps: true })
    .pipe(debug({ title: 'html:' }))
    .pipe(dest('build/'), { sourcemaps: true })
}

function clean () {
  return src('build/', { read: false, allowEmpty: true })
    .pipe(debug({ title: 'cleaning:' }))
    .pipe(gulpClean())
}

function watch () {
  return watchGulp('src/**/*', { ignoreInitial: false }, exports.default)
}

exports.default = parallel(css, js, html)
exports.watch = watch
exports.css = css
exports.js = js
exports.html = html
exports.clean = clean
