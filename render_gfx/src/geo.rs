use crate::render::Vertex;

pub enum Shapes {
    Quad,
    Cube,
    Octohedron,
}

pub trait Geometry {
    fn get_vertices(&self) -> &[Vertex];
    fn get_indices(&self) -> &[u16];
}

pub struct Quad {
    vertices: Vec<Vertex>,
    indices: Vec<u16>,
}

impl Quad {
    pub fn new(color: [f32; 4]) -> Self {
        Self {
            vertices: vec![
                Vertex {
                    pos: [-1.0, -1.0, 0.0],
                    color,
                },
                Vertex {
                    pos: [1.0, -1.0, 0.0],
                    color,
                },
                Vertex {
                    pos: [-1.0, 1.0, 0.0],
                    color,
                },
                Vertex {
                    pos: [1.0, 1.0, 0.0],
                    color,
                },
            ],
            #[rustfmt::skip]
            indices: vec![
                0, 1, 2,
                2, 1, 3,
            ],
        }
    }
}

impl Default for Quad {
    fn default() -> Self {
        Self::new([0.0, 0.0, 0.0, 1.0])
    }
}

impl Geometry for Quad {
    fn get_vertices(&self) -> &[Vertex] {
        &self.vertices[..]
    }

    fn get_indices(&self) -> &[u16] {
        &self.indices[..]
    }
}

pub struct Cube {
    vertices: Vec<Vertex>,
    indices: Vec<u16>,
}

impl Cube {
    pub fn new(color: [f32; 4]) -> Self {
        Self {
            vertices: vec![
                Vertex {
                    pos: [-1.0, -1.0, -1.0],
                    color,
                },
                Vertex {
                    pos: [1.0, -1.0, -1.0],
                    color,
                },
                Vertex {
                    pos: [-1.0, 1.0, -1.0],
                    color,
                },
                Vertex {
                    pos: [1.0, 1.0, -1.0],
                    color,
                },
                Vertex {
                    pos: [-1.0, -1.0, 1.0],
                    color,
                },
                Vertex {
                    pos: [1.0, -1.0, 1.0],
                    color,
                },
                Vertex {
                    pos: [-1.0, 1.0, 1.0],
                    color,
                },
                Vertex {
                    pos: [1.0, 1.0, 1.0],
                    color,
                },
            ],
            #[rustfmt::skip]
            indices: vec![
                // Front
                0, 1, 2, 2, 1, 3,
                // Left
                4, 0, 6, 6, 0, 2,
                // Back
                5, 4, 7, 7, 4, 6,
                // Right
                1, 5, 3, 3, 5, 7,
                // Bottom
                4, 5, 0, 0, 5, 1,
                // Top
                2, 3, 6, 6, 3, 7
            ],
        }
    }
}

impl Default for Cube {
    fn default() -> Self {
        Self::new([0.0, 0.0, 0.0, 1.0])
    }
}

impl Geometry for Cube {
    fn get_vertices(&self) -> &[Vertex] {
        &self.vertices[..]
    }

    fn get_indices(&self) -> &[u16] {
        &self.indices[..]
    }
}

pub struct Octohedron {
    vertices: Vec<Vertex>,
    indices: Vec<u16>,
}

impl Octohedron {
    pub fn new(color: [f32; 4]) -> Self {
        Self {
            vertices: vec![
                // Front
                Vertex {
                    pos: [0.0, 0.0, -1.0],
                    color,
                },
                // Left
                Vertex {
                    pos: [-1.0, 0.0, 0.0],
                    color,
                },
                // Back
                Vertex {
                    pos: [0.0, 0.0, 1.0],
                    color,
                },
                // Right
                Vertex {
                    pos: [1.0, 0.0, 0.0],
                    color,
                },
                // Top
                Vertex {
                    pos: [0.0, 1.0, 0.0],
                    color,
                },
                // Bottom
                Vertex {
                    pos: [0.0, -1.0, 0.0],
                    color,
                },
            ],
            ///    4      4
            ///   /|\    /|\
            ///  1-0-3  3-2-1
            ///   \|/    \|/
            ///    5      5
            #[rustfmt::skip]
            indices: vec![
                4, 0, 3, 3, 0, 5,
                4, 1, 0, 0, 1, 5,
                4, 2, 1, 1, 2, 5,
                4, 3, 2, 2, 3, 5,
            ],
        }
    }
}

impl Default for Octohedron {
    fn default() -> Self {
        Self::new([0.0, 0.0, 0.0, 1.0])
    }
}

impl Geometry for Octohedron {
    fn get_vertices(&self) -> &[Vertex] {
        &self.vertices[..]
    }

    fn get_indices(&self) -> &[u16] {
        &self.indices[..]
    }
}
