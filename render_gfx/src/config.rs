use std::path::PathBuf;

use clap::{Arg, ArgMatches, Command};

use data_mandel::{BlankStyle, BuddhabrotConfig, CacheSource, PaintStyle};

use crate::{chronos::ChronosMode, geo};

pub fn parse_raw_cli() -> ArgMatches {
    let cli_name = format!("Shader Playground: {}", env!("CARGO_BIN_NAME"));

    let mut about = format!(
        "A playground for making art.\n{} subsystem: {}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_DESCRIPTION")
    );
    let repo = env!("CARGO_PKG_REPOSITORY");
    if !repo.is_empty() {
        about += "\nRepo: ";
        about += repo;
    }

    let author_line = format!("By: {}", env!("CARGO_PKG_AUTHORS"));

    Command::new(cli_name)
        .author(author_line)
        .about(about)
        .arg(
            Arg::new("MODE")
                .short('m')
                .long("mode")
                .help("switch modes")
                .value_parser(["loop", "run", "once"])
                // .possible_values(&["loop", "run", "once"])
                .default_value("run")
                .num_args(1),
        )
        .arg(
            Arg::new("SHAPE")
                .long("shape")
                .help("choose geometry sent")
                .value_parser(["quad", "cube", "octohedron"])
                .default_value("quad")
                .num_args(1),
        )
        .arg(
            Arg::new("FPS")
                .short('f')
                .long("fps")
                .help("frames per render second")
                .value_parser(clap::value_parser!(i64))
                .default_value("60")
                .num_args(1),
        )
        .arg(
            Arg::new("RENDER_SECONDS")
                .short('r')
                .long("render-seconds")
                .help("number of render seconds")
                .value_parser(clap::value_parser!(i64))
                .default_value("10")
                .num_args(1),
        )
        .arg(
            Arg::new("TIME_OFFSET")
                .short('t')
                .long("time-offset")
                .help("number of render seconds after 0 to start")
                .value_parser(clap::value_parser!(i64))
                .default_value("0")
                .num_args(1),
        )
        .arg(
            Arg::new("WIDTH")
                .short('w')
                .long("width")
                .help("Sets the width")
                .value_parser(clap::value_parser!(u32))
                .default_value("1024")
                .num_args(1),
        )
        .arg(
            Arg::new("HEIGHT")
                .short('h')
                .long("height")
                .help("Sets the height")
                .value_parser(clap::value_parser!(u32))
                .default_value("1024")
                .num_args(1),
        )
        .arg(Arg::new("GIF").long("gif").help("save to gif").num_args(0))
        .arg(
            Arg::new("GIF_DELAY")
                .long("gif-delay")
                .help("centiseconds between gif frames")
                .num_args(1),
        )
        .arg(
            Arg::new("GIF_REPEAT")
                .long("gif-repeat")
                .help("number of times to repeat, -1 for infitite, otherwise must be positive")
                .num_args(1)
                .value_parser(-1..)
                .allow_hyphen_values(true),
        )
        .arg(
            Arg::new("TIMING")
                .long("timing")
                .help("show debug timings")
                .num_args(0),
        )
        .arg(Arg::new("PNG").long("png").help("save to png").num_args(0))
        .arg(
            Arg::new("PNG_BATCH")
                .long("png-batch")
                .help("save all frames to png")
                .num_args(0),
        )
        .arg(
            Arg::new("FLYWHEEL")
                .long("flywheel")
                .help("run as fast as possible")
                .num_args(0),
        )
        .arg(
            Arg::new("LINE")
                .long("line")
                .help("line drawing mode")
                .num_args(0),
        )
        .arg(
            Arg::new("NO_CULL")
                .long("no-cull")
                .help("turn off backface culling")
                .num_args(0),
        )
        .arg(
            Arg::new("GEN_TEXTURE")
                .long("gen-texture")
                .help("uses the current built in texture generator")
                .num_args(0),
        )
        .arg(
            Arg::new("TEXTURE_SIZE")
                .long("texture-size")
                .help("sets the squared size of the generated textures")
                .value_parser(clap::value_parser!(usize))
                .default_value("1024")
                .num_args(1),
        )
        .arg(
            Arg::new("BUDDHABROT_CACHE")
                .long("buddhabrot-cache")
                .help("sets the cache source for making buddhabrot")
                .default_value("grid")
                .value_parser(["grid", "file"])
                .num_args(1),
        )
        .arg(
            Arg::new("BUDDHABROT_FILE")
                .long("buddhabrot-file")
                .help("file to load ndjson for buddhabrot")
                .value_parser(clap::value_parser!(PathBuf))
                .default_value("data/order-7.ndjson")
                .num_args(1),
        )
        .arg(
            Arg::new("BUDDHABROT_PAINT")
                .long("buddhabrot-paint")
                .help("sets the paint style for buddhabrot")
                .default_value("merge")
                .value_parser(["density", "merge", "petalbrot"])
                .num_args(1),
        )
        .arg(
            Arg::new("BUDDHABROT_BLANK")
                .long("buddhabrot-blank")
                .help("sets the blank (clear) style for buddhabrot")
                .default_value("black")
                .value_parser(["black", "cyclefade", "greentobluefade"])
                .num_args(1),
        )
        .arg(
            Arg::new("BUDDHABROT_LIMIT")
                .long("buddhabrot-limit")
                .help("max number of mandelbrot iterations while processing")
                .value_parser(clap::value_parser!(usize))
                .num_args(1)
                .default_value("100000"),
        )
        .arg(
            Arg::new("FRAG_SHADER")
                .long("frag-shader")
                .help("file in shaders")
                .value_parser(clap::value_parser!(PathBuf))
                .num_args(1)
                .required(true),
        )
        .arg(
            Arg::new("VERT_SHADER")
                .long("vert-shader")
                .help("file in shaders")
                .value_parser(clap::value_parser!(PathBuf))
                .default_value("vert/passthrough_001.glslv")
                .num_args(1),
        )
        .get_matches()
}

pub struct RenderConfig {
    pub mode: ChronosMode,
    pub render_fps: i64,
    pub offset_s: i64,
    pub width: u32,
    pub height: u32,
    pub save_gif: bool,
    pub save_png: bool,
    pub debug_timing: bool,
    pub frag_shader_file: Option<PathBuf>,
    pub vert_shader_file: Option<PathBuf>,
    pub gif_frame_delay: Option<u16>,
    pub gif_repeat_type: Option<gif::Repeat>,
    pub flywheel: bool,
    pub line: bool,
    pub shape: geo::Shapes,
    pub cull: bool,
    pub gen_texture: bool,
    pub texture_size: usize,
    pub buddhabrot: Option<BuddhabrotConfig>,
}

impl Default for RenderConfig {
    fn default() -> Self {
        Self {
            mode: ChronosMode::Run,
            render_fps: 60,
            offset_s: 0,
            width: 1024,
            height: 1024,
            save_gif: false,
            save_png: false,
            debug_timing: false,
            frag_shader_file: None,
            vert_shader_file: None,
            gif_frame_delay: None,
            gif_repeat_type: None,
            flywheel: false,
            line: false,
            shape: geo::Shapes::Quad,
            cull: true,
            gen_texture: false,
            texture_size: 1024,
            buddhabrot: None,
        }
    }
}

pub fn parse_cli_to_buddhabrot(matches: &mut ArgMatches) -> BuddhabrotConfig {
    let data_file = matches.remove_one("BUDDHABROT_FILE").unwrap();

    let cache_source = match matches
        .remove_one::<String>("BUDDHABROT_CACHE")
        .unwrap()
        .as_str()
    {
        "grid" => CacheSource::GenGrid { jitter: false },
        "file" => CacheSource::LoadFile(data_file),
        _ => unreachable!("clap possible values makes this impossible"),
    };

    let paint_style = match matches
        .remove_one::<String>("BUDDHABROT_PAINT")
        .unwrap()
        .as_str()
    {
        "density" => PaintStyle::DensityScan,
        "merge" => PaintStyle::CachedMerge,
        // "petalbrot" => PaintStyle::Petalbrot,
        _ => unreachable!("clap possible values makes this impossible"),
    };

    let blank_style = match matches
        .remove_one::<String>("BUDDHABROT_BLANK")
        .unwrap()
        .as_str()
    {
        "black" => BlankStyle::Black,
        "cyclefade" => BlankStyle::CycleFade,
        "greentobluefade" => BlankStyle::GreenToBlueFade,
        _ => unreachable!("clap possible values makes this impossible"),
    };
    let limit = matches.remove_one("BUDDHABROT_LIMIT").unwrap();

    // let limit = value_t!(matches, "BUDDHABROT_LIMIT", usize).unwrap_or_else(|e| e.exit());

    BuddhabrotConfig {
        cache_source,
        paint_style,
        blank_style,
        limit,
    }
}

impl RenderConfig {
    pub fn parse_cli(matches: &mut ArgMatches) -> Self {
        let fps = matches.remove_one("FPS").unwrap();
        let seconds_to_render = matches.remove_one("RENDER_SECONDS").unwrap();
        let time_offset = matches.remove_one("TIME_OFFSET").unwrap();
        let width = matches.remove_one("WIDTH").unwrap();
        let height = matches.remove_one("HEIGHT").unwrap();
        let render_gif = matches.get_flag("GIF");
        let render_png = matches.get_flag("PNG");
        let print_timing = matches.get_flag("TIMING");
        let gen_texture = matches.get_flag("GEN_TEXTURE");

        let buddhabrot = {
            if gen_texture {
                Some(parse_cli_to_buddhabrot(matches))
            } else {
                None
            }
        };

        let (vert_shader_file, frag_shader_file) = (
            Some(matches.remove_one("VERT_SHADER").unwrap()),
            Some(matches.remove_one("FRAG_SHADER").unwrap()),
        );

        let mode: String = matches.remove_one("MODE").unwrap();
        let chronos_mode = ChronosMode::from_config(&mode, seconds_to_render);
        let gif_frame_delay = matches.remove_one("GIF_DELAY");
        let gif_repeat_type = matches.remove_one("GIF_REPEAT").map(|count| match count {
            0..=std::i16::MAX => gif::Repeat::Finite(count as u16),
            -1 => gif::Repeat::Infinite,
            _ => unreachable!("validator should prevent this"),
        });
        let flywheel = matches.get_flag("FLYWHEEL");
        let line = matches.get_flag("LINE");
        let cull = !matches.get_flag("NO_CULL");

        let shape = match matches.remove_one::<String>("SHAPE").unwrap().as_str() {
            "quad" => geo::Shapes::Quad,
            "cube" => geo::Shapes::Cube,
            "octohedron" => geo::Shapes::Octohedron,
            _ => unreachable!(),
        };
        let texture_size = matches.remove_one("TEXTURE_SIZE").unwrap();

        Self {
            mode: chronos_mode,
            render_fps: fps,
            offset_s: time_offset,
            width,
            height,
            save_gif: render_gif,
            save_png: render_png,
            debug_timing: print_timing,
            frag_shader_file,
            vert_shader_file,
            gif_frame_delay,
            gif_repeat_type,
            flywheel,
            line,
            shape,
            cull,
            gen_texture,
            texture_size,
            buddhabrot,
        }
    }
}
