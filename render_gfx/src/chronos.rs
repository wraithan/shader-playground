const NANOS_PER_SEC: i64 = 1_000_000_000;

#[derive(Clone, Copy, Debug)]
pub struct Chronos {
    pub render_step_ns: i64,
    pub display_step_ns: i64,
    // 0..
    current_time_s: i64,
    // 0..NANOS_PER_SEC
    current_time_ns: i64,
    // 0..
    rendered_ns: i64,
    // 0..
    start_time_s: i64,
    pub mode: ChronosMode,
}

impl Chronos {
    pub const fn new(mode: ChronosMode) -> Self {
        Self {
            render_step_ns: 0,
            display_step_ns: 0,
            current_time_s: 0,
            current_time_ns: 0,
            rendered_ns: 0,
            start_time_s: 0,
            mode,
        }
    }

    pub const fn render_fps(mut self, fps: i64) -> Self {
        self.render_step_ns = NANOS_PER_SEC / fps;
        self
    }

    pub const fn display_fps(mut self, fps: i64) -> Self {
        self.display_step_ns = NANOS_PER_SEC / fps;
        self
    }

    pub const fn skip_s(mut self, offset: i64) -> Self {
        self.start_time_s = offset;
        self.current_time_s = offset;
        self
    }

    pub fn reset_time(&mut self) {
        self.current_time_s = self.start_time_s;
    }

    pub fn tick(&mut self) -> Option<f32> {
        if let ChronosMode::Pause = self.mode {
            Some(self.get_render_s())
        } else {
            self.current_time_ns += self.render_step_ns;
            self.rendered_ns += i64::abs(self.render_step_ns);
            if self.current_time_ns < 0 {
                self.current_time_s += self.current_time_ns / NANOS_PER_SEC;
                self.current_time_ns = i64::abs(self.current_time_ns % NANOS_PER_SEC);
                if self.current_time_s < 0 {
                    self.current_time_s = i64::abs(self.current_time_s);
                    self.render_step_ns = i64::abs(self.render_step_ns);
                }
            } else if self.current_time_ns > NANOS_PER_SEC {
                self.current_time_s += self.current_time_ns / NANOS_PER_SEC;
                self.current_time_ns %= NANOS_PER_SEC;
            }
            match self.mode {
                ChronosMode::Once(limit_ns) => {
                    if self.rendered_ns > limit_ns {
                        None
                    } else {
                        Some(self.get_render_s())
                    }
                }
                ChronosMode::Loop(limit_ns) => {
                    if self.rendered_ns > limit_ns {
                        self.current_time_ns = 0;
                        self.current_time_s = self.start_time_s;
                        self.rendered_ns = 0;
                    }
                    Some(self.get_render_s())
                }
                ChronosMode::Run => Some(self.get_render_s()),
                ChronosMode::Pause => unreachable!(),
            }
        }
    }

    pub fn get_render_s(&self) -> f32 {
        (self.current_time_s as f32) + (self.current_time_ns as f32 / NANOS_PER_SEC as f32)
    }

    pub const fn get_render_step_as_whole_centiseconds(&self) -> u16 {
        (self.render_step_ns / (NANOS_PER_SEC / 100)) as u16
    }
}

#[derive(Clone, Copy, Debug)]
pub enum ChronosMode {
    Once(i64),
    Loop(i64),
    Run,
    Pause,
}

impl ChronosMode {
    pub fn from_config(mode: &str, seconds_to_render: i64) -> Self {
        match mode {
            "loop" => Self::Loop(seconds_to_render * NANOS_PER_SEC),
            "run" => Self::Run,
            "once" => Self::Once(seconds_to_render * NANOS_PER_SEC),
            _ => unreachable!(),
        }
    }
}
