#![deny(clippy::all)]
#![forbid(unsafe_code)]
#![deny(missing_debug_implementations, nonstandard_style)]
#![warn(future_incompatible, rust_2018_idioms)]

mod chronos;
mod config;
mod geo;
mod render;

use tracing::info;

use crate::{
    config::{parse_raw_cli, RenderConfig},
    render::render_art,
};

fn main() {
    tracing_subscriber::fmt()
        // .with_max_level(tracing::Level::TRACE)
        .with_env_filter("render_gfx=trace,data_mandel=debug,shader_script=trace")
        .with_level(true)
        .with_thread_names(true)
        .init();

    info!("Starting up!");

    let mut render_matches = parse_raw_cli();
    render_art(&RenderConfig::parse_cli(&mut render_matches));

    info!("Done!");
}
