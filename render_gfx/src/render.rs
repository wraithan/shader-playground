use std::{
    fs::OpenOptions,
    num::NonZeroU32,
    path::PathBuf,
    sync::atomic::{AtomicUsize, Ordering},
    thread,
    time::Duration,
};

use crossbeam_utils::sync::WaitGroup;
use gfx::{
    self,
    format::{self, Formatted, SurfaceTyped},
    gfx_constant_struct_meta, gfx_defines, gfx_impl_struct_meta, gfx_pipeline, gfx_pipeline_inner,
    gfx_vertex_struct_meta, handle,
    memory::{cast_slice, Bind, Typed, Usage},
    state::Rasterizer,
    texture,
    traits::{Factory, FactoryExt},
    CombinedError, Device,
};
use gif::{Encoder, Frame};

use glutin::surface::GlSurface;
use winit::{
    dpi::PhysicalSize,
    event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::EventLoop,
    platform::run_return::EventLoopExtRunReturn,
    window::WindowBuilder,
};

use rayon::prelude::*;
use tracing::{debug, info, trace};

use data_mandel::{build_texture_and_cache, iterate_texture};
use shader_script::ShaderScript;

use crate::chronos::{Chronos, ChronosMode};
use crate::config::RenderConfig;
use crate::geo::{self, Geometry};

type ColorFormat = gfx::format::Rgba8;
type DepthFormat = gfx::format::DepthStencil;
type SurfaceData = <<ColorFormat as Formatted>::Surface as SurfaceTyped>::DataType;

gfx_defines! {
    vertex Vertex {
        pos: [f32; 3] = "in_pos",
        color: [f32; 4] = "in_col",
    }

    constant Uniforms {
        resolution: [f32; 2] = "Uniforms.resolution",
        time_s: f32 = "Uniforms.time_s",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        texture_input_1: gfx::TextureSampler<[f32; 4]> = "Tex1",
        uniforms: gfx::ConstantBuffer<Uniforms> = "Uniforms",
        out: gfx::RenderTarget<ColorFormat> = "Target0",
    }
}

pub fn render_art(config: &RenderConfig) {
    let mut chronos = Chronos::new(config.mode)
        .render_fps(config.render_fps)
        .display_fps(config.render_fps)
        .skip_s(config.offset_s);
    let mut frames: Vec<Vec<u8>> = vec![];
    let wg = WaitGroup::new();

    inner_render(config, &mut chronos, &wg, &mut frames);

    if config.save_gif {
        let output_gif = OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open("test.gif")
            .expect("couldn't open test.gif for writing");
        let mut encoder_gif =
            Encoder::new(&output_gif, config.width as u16, config.height as u16, &[])
                .expect("failed to make gif encoder");

        if let Some(repeat_type) = config.gif_repeat_type {
            let repeat_type: gif::Repeat = repeat_type;
            encoder_gif
                .set_repeat(repeat_type)
                .expect("failed to set gif repeat type");
        }

        let calculated_frame_delay = u16::max(chronos.get_render_step_as_whole_centiseconds(), 4);

        let counter = AtomicUsize::new(0);
        let num_frames = frames.len();
        let frames: Vec<Frame<'_>> = frames
            .into_par_iter()
            .enumerate()
            .map(|(index, mut f)| {
                let current_count = counter.fetch_add(1, Ordering::Relaxed);
                info!(frame_num = index, progress = ?(current_count as f32 / num_frames as f32) * 100.0, "processing frame");
                // let mut f: Vec<u8> =
                //     f /*.chunks(w as usize * 4).rev().flatten().cloned().collect() */
                //         .clone();
                let mut frame = Frame::from_rgba(config.width as u16, config.height as u16, &mut f);

                // Frame delay is expressed in centiseconds in gifs
                frame.delay = config
                    .gif_frame_delay
                    .map_or_else(|| calculated_frame_delay, |delay| delay);
                frame
            })
            .collect();

        for frame in frames {
            encoder_gif
                .write_frame(&frame)
                .expect("failed to encode frame into gif");
        }
    }
    wg.wait();
}

fn inner_render(
    config: &RenderConfig,
    chronos: &mut Chronos,
    wg: &WaitGroup,
    frames: &mut Vec<Vec<u8>>,
) {
    trace!(chronos = ?chronos, "Chronos loaded");
    info!(
        resolution = ?(config.width, config.height),
        gif = config.save_gif,
        png = config.save_png,
        debug_timing = config.debug_timing,
        "Starting actual render"
    );
    let mut frame_durations = vec![];
    let mut keep_running = true;
    let w = config.width as u16;
    let h = config.height as u16;

    let my_size = config.texture_size;
    let mut my_render_target: Vec<[u8; 4]> = vec![[0; 4]; my_size * my_size];

    if let Some(buddhabrot_config) = &config.buddhabrot {
        if config.gen_texture {
            build_texture_and_cache(buddhabrot_config, &mut my_render_target, my_size);
        }
    }

    let mut events_loop = EventLoop::new();
    // let hidpi_factor = events_loop.primary_monitor().expect("should have primary monitor").scale_factor();
    let texture_width = config.width;
    let texture_height = config.height;
    let window_config = WindowBuilder::new()
        .with_title("Wraithan's Shader Playground")
        .with_inner_size(PhysicalSize::new(texture_width, texture_height));

    // Initialise winit window, glutin context & gfx views
    let old_school_gfx_glutin_ext::Init {
        // winit window
        window,
        // glutin bits
        // gl_config,
        gl_surface,
        gl_context,
        // gfx bits
        mut device,
        mut factory,
        color_view,
        mut depth_view,
        ..
    } = old_school_gfx_glutin_ext::window_builder(&events_loop, window_config)
        .build::<ColorFormat, DepthFormat>()
        .expect("Failed to create window");

    let mut encoder = gfx::Encoder::from(factory.create_command_buffer());

    let geometry: Box<dyn Geometry> = match config.shape {
        geo::Shapes::Cube => Box::<geo::Cube>::default(),
        geo::Shapes::Quad => Box::<geo::Quad>::default(),
        geo::Shapes::Octohedron => Box::<geo::Octohedron>::default(),
    };

    let (vertex_buffer, vertex_slice) =
        factory.create_vertex_buffer_with_slice(geometry.get_vertices(), geometry.get_indices());

    let mut uniforms = Uniforms {
        time_s: chronos.get_render_s(),
        resolution: [texture_width as f32, texture_height as f32],
    };

    let uniforms_buffer = factory.create_constant_buffer(1);

    let (actual_texture, texture_input_1): (_, gfx::handle::ShaderResourceView<_, [f32; 4]>) = {
        let size = my_size as gfx::texture::Size;
        let kind = gfx::texture::Kind::D2(size, size, gfx::texture::AaMode::Single);
        create_texture_dynamic::<gfx::format::Rgba8, _, _>(
            &mut factory,
            kind,
            gfx::texture::Mipmap::Provided,
            &[&my_render_target[..]],
        )
        .expect("failed to create texture")
    };

    let sampler = factory.create_sampler(gfx::texture::SamplerInfo::new(
        // gfx::texture::FilterMethod::Scale,
        // gfx::texture::FilterMethod::Bilinear,
        gfx::texture::FilterMethod::Trilinear,
        gfx::texture::WrapMode::Clamp,
    ));

    let mut data = pipe::Data {
        vbuf: vertex_buffer,
        uniforms: uniforms_buffer,
        out: color_view,
        texture_input_1: (texture_input_1, sampler),
    };

    let shaders_root = ShaderScript::get_shaders_root();
    let vert_shader = if let Some(ref file) = config.vert_shader_file {
        ShaderScript::load(&shaders_root, file)
    } else {
        unimplemented!("missing shader?");
    };

    let vert_shader_bytes: Vec<u8> = vert_shader.processed_source.bytes().collect();

    let frag_shader = if let Some(ref file) = config.frag_shader_file {
        ShaderScript::load(&shaders_root, file)
    } else {
        unimplemented!("missing shader?");
    };

    let frag_shader_bytes: Vec<u8> = frag_shader.processed_source.bytes().collect();

    let (save_sender, save_receiver) = crossbeam_channel::unbounded::<FrameData>();

    {
        trace!("Built Wait Group");
        let wg = wg.clone();
        thread::spawn(move || {
            trace!("Spawned thread!");
            for (frame_number, frame_data) in save_receiver.iter().enumerate() {
                let wg = wg.clone();
                rayon::spawn(move || {
                    save_frame(frame_number, frame_data);
                    drop(wg);
                });
            }
            drop(wg);
        });
    }

    let pso = {
        let program = factory
            .link_program(&vert_shader_bytes[..], &frag_shader_bytes[..])
            .expect("couldn't create pso program");

        let mut rasterizer = if config.line {
            Rasterizer {
                method: gfx::state::RasterMethod::Line(1),
                ..Rasterizer::new_fill()
            }
        } else {
            Rasterizer::new_fill()
        };

        if config.cull {
            rasterizer = rasterizer.with_cull_back();
        }

        factory
            .create_pipeline_from_program(
                &program,
                gfx::Primitive::TriangleList,
                rasterizer,
                pipe::new(),
            )
            .expect("failed to init PSO")
    };

    let min_frame_duration = Duration::from_nanos(chronos.display_step_ns as u64);

    {
        let (nw, nh, _, _) = data.out.get_dimensions();
        assert_eq!(u32::from(nw), config.width);
        assert_eq!(u32::from(nh), config.height);
    }
    let mut download = factory
        .create_download_buffer::<SurfaceData>(w as usize * h as usize)
        .expect("Failed to make download buffer");

    let mut raw_image_info = gfx::texture::RawImageInfo {
        xoffset: 0,
        yoffset: 0,
        zoffset: 0,
        width: w,
        height: h,
        depth: 0,
        format: ColorFormat::get_format(),
        mipmap: 0,
    };

    let tex_info = gfx::texture::ImageInfoCommon {
        xoffset: 0,
        yoffset: 0,
        zoffset: 0,
        width: my_size as u16,
        height: my_size as u16,
        depth: 0,
        format: (),
        mipmap: 0,
    };

    let mut take_screenshot = false;

    let mut dimensions = window.inner_size();

    while let Some(time_s) = chronos.tick() {
        events_loop.run_return(|event, _window_target, flow| {
            *flow = winit::event_loop::ControlFlow::Exit;
            if let Event::WindowEvent { event, .. } = event {
                match event {
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(VirtualKeyCode::Q),
                                state: ElementState::Released,
                                ..
                            },
                        ..
                    } => keep_running = false,
                    WindowEvent::Resized(size) => {
                        // Let winit notify glutin/gfx that windows changed size
                        let window_size = window.inner_size();
                        if dimensions != window_size {
                            if let (Some(w), Some(h)) = (
                                NonZeroU32::new(window_size.width),
                                NonZeroU32::new(window_size.height),
                            ) {
                                gl_surface.resize(&gl_context, w, h);
                                old_school_gfx_glutin_ext::resize_views(
                                    window_size,
                                    &mut data.out,
                                    &mut depth_view,
                                );
                            }
                            dimensions = window_size;
                        }

                        // Resize buffer for reading from CPU side
                        let (w, h, _, _) = data.out.get_dimensions();
                        download = factory
                            .create_download_buffer::<SurfaceData>(w as usize * h as usize)
                            .expect("Failed to make download buffer");

                        raw_image_info = gfx::texture::RawImageInfo {
                            xoffset: 0,
                            yoffset: 0,
                            zoffset: 0,
                            width: w,
                            height: h,
                            depth: 0,
                            format: ColorFormat::get_format(),
                            mipmap: 0,
                        };
                        uniforms.resolution = [f32::from(w), f32::from(h)];
                        info!(?size, "Window resized");
                    }
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(VirtualKeyCode::Space),
                                state: ElementState::Released,
                                ..
                            },
                        ..
                    } => {
                        match chronos.mode {
                            ChronosMode::Pause => chronos.mode = config.mode,
                            _ => chronos.mode = ChronosMode::Pause,
                        };
                        info!(?chronos, "Pause");
                    }
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(VirtualKeyCode::P),
                                state: ElementState::Released,
                                ..
                            },
                        ..
                    } => take_screenshot = true,
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(VirtualKeyCode::R),
                                state: ElementState::Released,
                                ..
                            },
                        ..
                    } => chronos.reset_time(),
                    _ => (),
                }
            }
        });
        if !keep_running {
            break;
        }
        let frame_start_time = std::time::Instant::now();
        uniforms.time_s = time_s;
        if config.debug_timing {
            debug!(?uniforms.time_s);
        }
        // do texture stuff

        if let Some(buddhabrot_config) = &config.buddhabrot {
            if config.gen_texture {
                iterate_texture(buddhabrot_config, &mut my_render_target, my_size, time_s);
            }
        }
        encoder
            .update_buffer(&data.uniforms, &[uniforms], 0)
            .expect("failed to update uniform buffer");
        encoder
            .update_texture::<gfx::format::R8_G8_B8_A8, gfx::format::Rgba8>(
                &actual_texture,
                None,
                tex_info,
                &my_render_target[..],
            )
            .expect("failed up update texture_input_1");

        encoder.clear(&data.out, [0.0, 0.0, 0.0, 1.0]);
        // encoder.clear(&data.out, [0.0; 4]);
        encoder.draw(&vertex_slice, &pso, &data);
        if config.save_gif || config.save_png || take_screenshot {
            encoder
                .copy_texture_to_buffer_raw(
                    data.out.raw().get_texture(),
                    None,
                    raw_image_info,
                    download.raw(),
                    0,
                )
                .expect("failed to download texture");
        }
        encoder.flush(&mut device);
        gl_surface.swap_buffers(&gl_context).unwrap();
        device.cleanup();

        if config.save_gif || config.save_png || take_screenshot {
            let reader = factory
                .read_mapping(&download)
                .expect("failed to create download reader");

            let mut texture_buffer: Vec<u8> = Vec::with_capacity(raw_image_info.get_byte_count());

            for pixel in reader.iter() {
                texture_buffer.extend(pixel);
            }

            if config.save_gif {
                frames.push(texture_buffer.clone());
            }
            if config.save_png || take_screenshot {
                save_sender
                    .send(FrameData {
                        texture_buffer,
                        encode_as: SaveEncoding {
                            png: config.save_png || take_screenshot,
                        },
                        save_path: frag_shader.get_save_dir(),
                        filename: frag_shader.get_save_filename(),
                        width: w,
                        height: h,
                    })
                    .expect("failed to send frame data to save channel");
            }
            take_screenshot = false;
        }

        let delta = frame_start_time.elapsed();
        if config.debug_timing {
            debug!(?min_frame_duration, ?delta);
        }
        if !config.flywheel && delta < min_frame_duration {
            std::thread::sleep(min_frame_duration - delta);
        }
        frame_durations.push(delta);
    }
    drop(save_sender);

    let average_duration = frame_durations
        .iter()
        .fold(Duration::from_secs(0), |acc, cur| acc + *cur)
        .div_f32(frame_durations.len() as f32);

    info!(?average_duration, "Done rendering",);

    if !keep_running {
        info!("Window closed");
    }
}

struct SaveEncoding {
    pub png: bool,
}

struct FrameData {
    pub texture_buffer: Vec<u8>,
    pub encode_as: SaveEncoding,
    pub save_path: PathBuf,
    pub filename: PathBuf,
    pub width: u16,
    pub height: u16,
}

fn save_frame(frame_number: usize, mut frame_data: FrameData) {
    info!(
        frame_number,
        size = frame_data.texture_buffer.len(),
        "Evaluating frame to be saved",
    );

    if frame_data.encode_as.png {
        frame_data
            .save_path
            .push(format!("{}x{}", frame_data.width, frame_data.height));

        std::fs::create_dir_all(&frame_data.save_path).expect("couldn't create save dir");

        info!(frame_number, "Saving frame as PNG");
        let frame: Vec<u8> = frame_data
            .texture_buffer
            .chunks(frame_data.width as usize * 4)
            .rev()
            .flatten()
            .copied()
            .collect();

        image::save_buffer(
            frame_data.save_path.join(format!(
                "{}-{:0>4}.png",
                frame_data.filename.to_string_lossy(),
                frame_number
            )),
            // format!("output/test-{:0>4}.png", index),
            &frame,
            u32::from(frame_data.width),
            u32::from(frame_data.height),
            image::ColorType::Rgba8,
        )
        .expect("failed to encode frame into png");
    }
}

// Copied out of GFX so I could modify them, probably better as an additional
// Factory trait, mostly I just wanted `Usage::Dynamic`
#[allow(clippy::type_complexity)]
fn create_texture_dynamic_u8<T: format::TextureFormat, R: gfx::Resources, F: Factory<R>>(
    factory: &mut F,
    kind: texture::Kind,
    mipmap: texture::Mipmap,
    data: &[&[u8]],
) -> Result<
    (
        handle::Texture<R, T::Surface>,
        handle::ShaderResourceView<R, T::View>,
    ),
    CombinedError,
> {
    let surface = <T::Surface as format::SurfaceTyped>::get_surface_type();
    let num_slices = kind.get_num_slices().unwrap_or(1) as usize;
    let num_faces = if kind.is_cube() { 6 } else { 1 };
    let levels = match mipmap {
        texture::Mipmap::Allocated => {
            if data.len() == (num_slices * num_faces) {
                kind.get_num_levels()
            } else {
                return Err(CombinedError::Texture(texture::CreationError::Level(
                    (num_slices * num_faces) as texture::Level,
                )));
            }
        }
        texture::Mipmap::Provided => (data.len() / (num_slices * num_faces)) as texture::Level,
    };
    let desc = texture::Info {
        kind,
        levels,
        format: surface,
        bind: Bind::SHADER_RESOURCE,
        usage: Usage::Dynamic,
    };
    let cty = <T::Channel as format::ChannelTyped>::get_channel_type();
    let raw = factory.create_texture_raw(desc, Some(cty), Some((data, mipmap)))?;
    let levels = (0, raw.get_info().levels - 1);
    let tex = Typed::new(raw);
    let view =
        factory.view_texture_as_shader_resource::<T>(&tex, levels, format::Swizzle::new())?;
    Ok((tex, view))
}

#[allow(clippy::type_complexity)]
pub fn create_texture_dynamic<T: format::TextureFormat, R: gfx::Resources, F: Factory<R>>(
    factory: &mut F,
    kind: texture::Kind,
    mipmap: texture::Mipmap,
    data: &[&[<T::Surface as format::SurfaceTyped>::DataType]],
) -> Result<
    (
        handle::Texture<R, T::Surface>,
        handle::ShaderResourceView<R, T::View>,
    ),
    CombinedError,
> {
    // we can use cast_slice on a 2D slice, have to use a temporary array of slices
    let mut raw_data: [&[u8]; 0x100] = [&[]; 0x100];
    assert!(data.len() <= raw_data.len());
    for (rd, d) in raw_data.iter_mut().zip(data.iter()) {
        *rd = cast_slice(d);
    }
    create_texture_dynamic_u8::<T, R, F>(factory, kind, mipmap, &raw_data[..data.len()])
}
