#![deny(clippy::all)]
#![forbid(unsafe_code)]
#![deny(missing_debug_implementations, nonstandard_style)]
#![warn(future_incompatible, rust_2018_idioms)]

mod buddhabrot;
mod mandelbrot;
mod points;

use std::path::Path;

use tracing::warn;

pub use crate::{
    buddhabrot::{
        build_texture_and_cache, iterate_texture, BlankStyle, BuddhabrotConfig, CacheSource,
        PaintStyle,
    },
    mandelbrot::{compute_mandelbrot, MandelData, MandelState},
    points::{load_data_from_file, InterestingPoint},
};

#[inline]
pub fn get_order(input: usize) -> usize {
    ((input as f64).log10().floor() as usize) + 1
}

pub fn order_from_filename(filename: &Path) -> Result<u8, String> {
    // Gets the filename, splits it on - and grabs the second part and assumes that an int
    // Expected input: order-4.ndjson
    let order = filename
        .file_stem()
        .ok_or_else(|| "No file stem on asset?".to_string())?
        .to_str()
        .ok_or_else(|| "not a str?".to_string())?
        .split_once('-')
        .ok_or_else(|| "couldn't split?".to_string())?
        .1
        .parse::<u8>()
        .map_err(|err| format!("wasn't a number? {err:?}"))?;
    Ok(order)
}

#[cfg(test)]
mod test {
    use super::get_order;

    #[test]
    fn order_counts_digits() {
        assert_eq!(1, get_order(1));
        assert_eq!(1, get_order(9));
        assert_eq!(2, get_order(10));
        assert_eq!(2, get_order(99));
        assert_eq!(3, get_order(100));
        assert_eq!(3, get_order(999));
        assert_eq!(4, get_order(1000));
        assert_eq!(4, get_order(9999));
        assert_eq!(5, get_order(10000));
        assert_eq!(5, get_order(99999));
        assert_eq!(6, get_order(100000));
        assert_eq!(6, get_order(999999));
        assert_eq!(7, get_order(1000000));
        assert_eq!(7, get_order(9999999));
        assert_eq!(8, get_order(10000000));
        assert_eq!(8, get_order(99999999));
        assert_eq!(9, get_order(100000000));
        assert_eq!(9, get_order(999999999));
    }
}
