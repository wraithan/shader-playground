use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum MandelState {
    Inside,
    Outside,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MandelData {
    pub coordinates: (f64, f64),
    pub state: MandelState,
    pub data: Vec<(f64, f64)>,
}

pub fn compute_mandelbrot(
    real: f64,
    imaginary: f64,
    limit: usize,
    size_hint: Option<usize>,
) -> MandelData {
    {
        // skip main cardioid and period 2 bulb (the biggest circle)
        let p = (real - 0.25).hypot(imaginary);
        if real <= p - (2.0 * p * p) + 0.25
            || imaginary.mul_add(imaginary, (real + 1.0) * (real + 1.0)) <= 1.0 / 16.0
        {
            return MandelData {
                coordinates: (real, imaginary),
                state: MandelState::Inside,
                data: Vec::with_capacity(0),
            };
        }
    }
    let mut i_real = 0.0;
    let mut i_imaginary = 0.0;
    let mut result = MandelData {
        coordinates: (real, imaginary),
        state: MandelState::Inside,
        // data: Vec::new(),
        data: match size_hint {
            Some(size_hint) => Vec::with_capacity(usize::min(limit, size_hint)),
            None => vec![],
        },
    };
    for _ in 0..limit {
        let x = (i_real * i_real) - (i_imaginary * i_imaginary) + real;
        let y = (i_real * i_imaginary).mul_add(2.0, imaginary);
        i_real = x;
        i_imaginary = y;
        result.data.push((i_real, i_imaginary));
        if i_real.mul_add(i_real, i_imaginary * i_imaginary) > 4.0 {
            result.state = MandelState::Outside;
            break;
        }
    }
    result.data.shrink_to_fit();
    // if let Some(size_hint) = size_hint {
    //     let actual = result.data.len();
    //     if size_hint != actual {
    //         tracing::warn!(size_hint, actual, "Got mismatched hint from actual?");
    //     }
    // }
    result
}
