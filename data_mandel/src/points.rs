use std::{path::Path, time::Instant};

use serde::{Deserialize, Serialize};
use tracing::debug;

use crate::mandelbrot::{compute_mandelbrot, MandelData};

#[derive(Debug, Serialize, Deserialize, Copy, Clone)]
pub struct InterestingPoint {
    pub coordinates: (f64, f64),
    pub iterations: usize,
}

pub fn load_data_from_file<P: AsRef<Path>>(data_file: &P, sort: bool) -> Vec<InterestingPoint> {
    let start = Instant::now();
    let mut data: Vec<InterestingPoint> = std::fs::read_to_string(data_file)
        .expect("couldn't open data file")
        .lines()
        // .take(1000)
        .map(|s| serde_json::from_str(s).expect("couldn't parse line"))
        .collect();

    if sort {
        data.sort_by(|a, b| b.iterations.cmp(&a.iterations));
        // data.sort_by(|a, b| ((b.coordinates.0 * 1000.0) as isize).cmp(&((a.coordinates.0 * 1000.0) as isize)));
        // data.sort_by(|a, b| ((b.coordinates.1 * 1000.0) as isize).cmp(&((a.coordinates.1 * 1000.0) as isize)));
    }

    // data.truncate(1000);
    // let mut rng = &mut rand::thread_rng();
    // let data: Vec<InterestingPoint> = data.choose_multiple(&mut rng, 250).cloned().collect();
    debug!(duration = ?start.elapsed(), points = data.len(), "Loaded data from file");
    data
}

impl From<&MandelData> for InterestingPoint {
    fn from(mandel_data: &MandelData) -> Self {
        Self {
            coordinates: mandel_data.coordinates,
            iterations: mandel_data.data.len(),
        }
    }
}

impl InterestingPoint {
    pub fn compute_mandelbrot(&self) -> MandelData {
        compute_mandelbrot(
            self.coordinates.0,
            self.coordinates.1,
            self.iterations * 2,
            Some(self.iterations),
        )
    }
}
