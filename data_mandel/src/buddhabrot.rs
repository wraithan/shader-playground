use std::{
    path::{Path, PathBuf},
    sync::{
        atomic::{AtomicUsize, Ordering},
        Mutex,
    },
};

use lazy_static::lazy_static;
use rand::Rng;
use rayon::prelude::*;
use tracing::{info, trace};

use crate::{compute_mandelbrot, points, InterestingPoint, MandelData, MandelState};

type Coord = (f64, f64);
lazy_static! {
    static ref NEW_BUDDHA_CACHE: Mutex<Vec<MandelData>> = Mutex::default();
}

#[derive(Debug)]
pub enum CacheSource {
    GenGrid { jitter: bool },
    LoadFile(PathBuf),
    DataLoad,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum PaintStyle {
    CachedMerge,
    // Petalbrot,
    CachedDataOrigin,
    Density,
    DensityScan,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum BlankStyle {
    NoFade,
    Black,
    CycleFade,
    GreenToBlueFade,
    GreenToRedFade,
}

#[derive(Debug)]
pub struct BuddhabrotConfig {
    pub cache_source: CacheSource,
    pub paint_style: PaintStyle,
    pub blank_style: BlankStyle,
    pub limit: usize,
}

fn build_buddhabrot_grid(row_length: usize, limit: usize, jitter: bool) {
    info!(
        row_length,
        limit, jitter, "Generating points for buddhabrot along grid lines",
    );
    let start = std::time::Instant::now();
    // let counter = Mutex::new(0);
    let counter = AtomicUsize::new(0);

    let quarter_row = (row_length / 4) as f64;
    (0..row_length).into_par_iter().for_each(|row| {
        let row_start = std::time::Instant::now();
        let mut rng = rand::thread_rng();
        let mut row_val = row as f64 / quarter_row - 2.0;
        let mut to_cache = vec![];
        (0..row_length).for_each(|col| {
            let mut col_val = col as f64 / quarter_row - 2.0;

            if jitter {
                // increase epsilon for more variation
                let epsilon = (1.0 / row_length as f64) * 4.0;
                let x_jitter = (rng.gen::<f64>() * 2.0 * epsilon) - epsilon;
                let y_jitter = (rng.gen::<f64>() * 2.0 * epsilon) - epsilon;
                row_val += x_jitter;
                col_val += y_jitter;
            }

            let mandel_data = compute_mandelbrot(row_val, col_val, limit, None);
            if let MandelState::Outside = mandel_data.state {
                // Removes the glow around mandelbrot
                let data_len = mandel_data.data.len();
                if data_len < 10 {
                    return;
                }
                let reflection = compute_mandelbrot(row_val, col_val * -1.0, limit, Some(data_len));
                {
                    // println!("Found {:?} with len: {}", mandel_data.coordinates, mandel_data.data.len());
                    to_cache.push(mandel_data);
                    to_cache.push(reflection);
                }
            }
        });
        {
            let num_found = to_cache.len();
            let mut cache = NEW_BUDDHA_CACHE.lock().unwrap();
            cache.extend(to_cache);
            let current_count = counter.fetch_add(1, Ordering::Relaxed);
            trace!(
                counter = current_count,
                ?row,
                row_time = ?row_start.elapsed(),
                ?num_found
            );
        }
    });

    let cache = NEW_BUDDHA_CACHE.lock().unwrap();

    info!(
        limit,
        duration = ?start.elapsed(),
        cache_size = cache.len(),
        "Generated buddhabrot cache from grid"
    );
}

fn build_buddhabrot_file<P: AsRef<Path>>(data_file: &P, limit: usize) {
    let input_data = points::load_data_from_file(data_file, true);
    info!(limit, "Building buddhabrot cache from file with limit");

    build_buddhabrot_cache_from_points(&input_data, limit);
}

fn build_buddhabrot_cache_from_points(input_data: &[InterestingPoint], limit: usize) {
    let start = std::time::Instant::now();
    input_data.par_iter().for_each(|point| {
        let (x, y) = point.coordinates;
        // dbg!((*x, *y));
        let reg = compute_mandelbrot(x, y, limit, Some(point.iterations));
        if let MandelState::Outside = reg.state {
            let flip = compute_mandelbrot(x, y * -1.0, limit, Some(reg.data.len()));
            {
                let mut cache = NEW_BUDDHA_CACHE.lock().unwrap();
                cache.push(reg);
                cache.push(flip);
            }
        }
    });
    let cache_size = { NEW_BUDDHA_CACHE.lock().unwrap().len() };
    info!(duration = ?start.elapsed(), cache_size, input_len = input_data.len(), "Done building buddhabrot cache");
}

fn paint_buddhabrot_density<'a>(
    cache: impl IntoIterator<Item = &'a MandelData>,
    texture: &mut [[u8; 4]],
    row_length: usize,
) {
    info!("Painting buddhabrot density plot");
    let start = std::time::Instant::now();
    let quarter_row = (row_length / 4) as f64;
    let mut history = vec![0.0_f64; row_length * row_length];
    for MandelData {
        coordinates, data, ..
    } in cache
    {
        // draw source of mandel data
        if let Some(index) = coordinate_to_texel_index(*coordinates, row_length, quarter_row) {
            texture[index][0] = 128;
        }

        for data_point in data {
            if let Some(index) = coordinate_to_texel_index(*data_point, row_length, quarter_row) {
                history[index] += 1.0;
            }
        }
    }
    let biggest = history.iter().fold(0.0, |a, b| f64::max(a, *b));

    for (real, row) in history.chunks(row_length).enumerate() {
        for (imaginary, value) in row.iter().enumerate() {
            if *value > biggest * 0.01 {
                let index = (real * row_length) + imaginary;
                texture[index][1] = (((value / biggest) * 256.0) as u8).saturating_mul(3);
            }
        }
    }

    info!(
        texture_size = texture.len(),
        ?biggest,
        duration = ?start.elapsed(),
        "Painted buddhabrot as density plot"
    );
}

fn paint_buddhabrot_density_scan<'a>(
    cache: impl IntoIterator<Item = &'a MandelData>,
    texture: &mut [[u8; 4]],
    row_length: usize,
    time: f64,
) {
    info!("Painting buddhabrot density plot from cache");
    let start = std::time::Instant::now();
    let mut cache: Vec<&MandelData> = cache.into_iter().collect();

    cache.sort_unstable_by_key(|data| (data.coordinates.0 * 1000.0) as isize);
    // Limit cache lock to here.
    let bucket = cache.len() / 100;
    let amount_to_skip = (bucket as f64 * time) as usize;
    let to_paint: Vec<&MandelData> = cache
        .iter()
        .cycle()
        .skip(amount_to_skip)
        .take(bucket)
        .copied()
        .collect();
    info!(amount_to_skip, bucket, "painting!");

    paint_buddhabrot_density(to_paint, texture, row_length);

    info!(
        texture_size = texture.len(),
        duration = ?start.elapsed(),
        "Painted buddhabrot scan"
    );
}

fn paint_buddhabrot_mandelbrot_merge<'a>(
    cache: impl IntoIterator<Item = &'a MandelData>,
    texture: &mut [[u8; 4]],
    row_length: usize,
    limit: usize,
    time: f64,
) {
    info!("Painting buddhabrot -> mandelbrot merge from cache");
    let start = std::time::Instant::now();
    let data_start = 0;
    let quarter_row = (row_length / 4) as f64;
    // let mut rng = rand::thread_rng();
    let mut cache_size = 0;
    for mandel_data in cache {
        cache_size += 1;
        // let series_len = mandel_data.data.len();
        for (step_num, data_point) in mandel_data.data.iter().skip(data_start).enumerate() {
            // let t_offset = (rng.gen::<f64>() * 0.06) - 0.03;
            let data_point = point_lerp(*data_point, mandel_data.coordinates, time);
            if let Some(index) = coordinate_to_texel_index(data_point, row_length, quarter_row) {
                // if index > texture.len() {
                //     dbg!((x, y, row_length, texture.len(), index));
                //     continue;
                // }

                // if series_len < limit / 100 {
                //     texture[index][2] = texture[index][2].saturating_add(1);
                // } else if series_len < limit / 10 {
                //     // } else if series.len() < (limit / 10) * 6 {
                //     texture[index][1] = texture[index][1].saturating_add(1);
                // } else {
                //     texture[index][0] = texture[index][0].saturating_add(2);
                // }

                if step_num < limit / 100 {
                    texture[index][1] = u8::min(texture[index][1].saturating_add(1), 192);
                // } else if step_num < limit / 10 {
                //     texture[index][1] = texture[index][1].saturating_add(2);
                } else {
                    texture[index][1] = u8::min(texture[index][1].saturating_add(2), 192);
                    texture[index][2] = texture[index][2].saturating_add(1);
                }

                // let diff_x = mandel_data.coordinates.0 - x;
                // let diff_y = mandel_data.coordinates.1 - y;
                // let dist_from_origin = (diff_x.powf(2.0) + diff_y.powf(2.0)).sqrt();

                // if dist_from_origin > 0.1 + (t_offset * 4.0) {
                //     texture[index][2] = u8::min(texture[index][2].saturating_add(1), 128);
                // } else {
                //     texture[index][2] = u8::min(texture[index][2].saturating_add(2), 128);
                //     texture[index][1] = u8::min(texture[index][1].saturating_add(1), 160);
                // }
            }
        }
    }

    info!(
        texture_size = texture.len(),
        limit,
        cache_size,
        duration = ?start.elapsed(),
        "Painted buddhabrot merge"
    );
}

fn paint_data_origin<'a>(
    cache: impl IntoIterator<Item = &'a MandelData>,
    texture: &mut [[u8; 4]],
    row_length: usize,
) {
    info!("Painting cache data origin");
    let start = std::time::Instant::now();
    let quarter_row = (row_length / 4) as f64;
    let mut cache_size = 0;
    for mandel_data in cache {
        cache_size += 1;
        if let Some(index) =
            coordinate_to_texel_index(mandel_data.coordinates, row_length, quarter_row)
        {
            texture[index][0] = texture[index][0].saturating_add(1);
            texture[index][1] = texture[index][1].saturating_add(1);
            texture[index][2] = texture[index][2].saturating_add(1);
        }
    }

    info!(
        texture_size = texture.len(),
        cache_size,
        duration = ?start.elapsed(),
        "Painted cache data origin"
    );
}

#[allow(dead_code)]
fn paint_build_petalbrot(texture: &mut [[u8; 4]], row_length: usize) {
    for (row_index, row) in texture.chunks_mut(row_length).enumerate() {
        let row_val = (row_index as f64 / (row_length / 4) as f64) - 2.0;
        for (col_index, col) in row.iter_mut().enumerate() {
            let col_val = (col_index as f64 / (row_length / 4) as f64) - 2.0;
            let mut i_row_val = 0.0;
            let mut i_col_val = 0.0;
            col[0] = 128;
            for step in 0..=255 {
                let x = (i_row_val * i_row_val) - (i_col_val * i_col_val) + row_val;
                let y = (i_row_val * i_col_val).mul_add(2.0, col_val);
                i_row_val = x;
                i_col_val = y;
                if i_row_val.mul_add(i_row_val, i_col_val * i_col_val) > 4.0 {
                    // if (i_row_val * i_col_val) > 4.0 {
                    col[1] = step % 128;
                    col[0] = 0;
                    break;
                }
            }
        }
    }
}

pub fn build_texture_and_cache(
    config: &BuddhabrotConfig,
    texture: &mut [[u8; 4]],
    row_length: usize,
) {
    texture.iter_mut().for_each(|texel| texel[3] = 255);

    build_cache(config, row_length, None);
}

fn build_cache(config: &BuddhabrotConfig, row_length: usize, data: Option<&[InterestingPoint]>) {
    match &config.cache_source {
        // TODO: Make row_length a enum param
        CacheSource::GenGrid { jitter } => build_buddhabrot_grid(row_length, config.limit, *jitter),
        CacheSource::LoadFile(data_file) => build_buddhabrot_file(data_file, config.limit),
        CacheSource::DataLoad => build_buddhabrot_cache_from_points(
            data.expect("Data load should provide data"),
            config.limit,
        ),
    }
}

pub fn iterate_texture(
    config: &BuddhabrotConfig,
    texture: &mut [[u8; 4]],
    row_length: usize,
    time: f32,
) {
    let cache = NEW_BUDDHA_CACHE.lock().unwrap();
    config.blank_style.blank_texture(texture);
    config
        .paint_style
        .paint_texture(cache.iter(), texture, row_length, time, config.limit);
}

impl BlankStyle {
    pub fn blank_texture(&self, texture: &mut [[u8; 4]]) {
        match *self {
            BlankStyle::NoFade => (),
            BlankStyle::Black => {
                for texel in texture.iter_mut() {
                    texel[0] = 0;
                    texel[1] = 0;
                    texel[2] = 0;
                }
            }
            BlankStyle::CycleFade => {
                for texel in texture.iter_mut() {
                    let texel0 = texel[0];
                    texel[0] = (texel[0] / 6).saturating_add(texel[2] / 2); // Red
                    texel[2] = (texel[2] / 6).saturating_add(texel[1] / 2); // Blue
                    texel[1] = (texel[1] / 6).saturating_add(texel0 / 2); // Green
                }
            }
            BlankStyle::GreenToBlueFade => {
                for texel in texture.iter_mut() {
                    texel[0] /= 3;
                    texel[2] = ((texel[1] / 5) * 4).saturating_add((texel[2] / 9) * 8);
                    texel[1] /= 2;
                }
            }
            BlankStyle::GreenToRedFade => {
                for texel in texture.iter_mut() {
                    texel[2] /= 3;
                    texel[0] = ((texel[1] / 5) * 4).saturating_add((texel[2] / 9) * 8);
                    texel[1] /= 2;
                }
            }
        }
    }
}

impl PaintStyle {
    pub fn paint_texture<'a>(
        &self,
        data: impl IntoIterator<Item = &'a MandelData>,
        texture: &mut [[u8; 4]],
        row_length: usize,
        time: f32,
        limit: usize,
    ) {
        match *self {
            PaintStyle::CachedMerge => {
                paint_buddhabrot_mandelbrot_merge(data, texture, row_length, limit, f64::from(time))
            }
            // PaintStyle::Petalbrot => paint_build_petalbrot(texture, row_length),
            PaintStyle::CachedDataOrigin => paint_data_origin(data, texture, row_length),
            PaintStyle::Density => paint_buddhabrot_density(data, texture, row_length),
            PaintStyle::DensityScan => {
                paint_buddhabrot_density_scan(data, texture, row_length, f64::from(time))
            }
        }
    }
}

fn point_lerp(a: Coord, b: Coord, t: f64) -> Coord {
    (t.mul_add(b.0 - a.0, a.0), t.mul_add(b.1 - a.1, a.1))
}

/// Converts floating coords to offset in texture vector
///
/// Returns `None` if either of `x` or `y` is not inside of -2.0 to 2.0
///
/// `quarter_row` is passed in as a caching thing to not have to keep convert to float and dividing, needs profiling.
fn coordinate_to_texel_index(
    (x, y): (f64, f64),
    row_length: usize,
    quarter_row: f64,
) -> Option<usize> {
    if let (Some(texel_x), Some(texel_y)) = (
        rescale_shift_from_signed(x, quarter_row),
        rescale_shift_from_signed(y, quarter_row),
    ) {
        Some((texel_x * row_length) + texel_y)
    } else {
        None
    }
}

/// Used to take a floating x or y on the imaginary plane and convert to unsigned integer.
///
/// Returns None if `f` is not inside of -2.0 to 2.0
fn rescale_shift_from_signed(f: f64, quarter_row: f64) -> Option<usize> {
    if f <= -2.0 || f >= 2.0 {
        None
    } else {
        Some(((f + 2.0) * quarter_row) as usize)
    }
}
