use std::path::PathBuf;

use clap::Parser;

use shader_script::ShaderScript;

/// Preprocesses a shader file and outputs it to stdout.
#[derive(Debug, Parser)]
struct CompileOptions {
    /// Path to shader to load and compile
    shader: PathBuf,
}

fn main() {
    let compile_opts = CompileOptions::parse();
    let shaders_root = ShaderScript::get_shaders_root();
    let shader = ShaderScript::load(&shaders_root, &compile_opts.shader);
    println!("{}", shader.processed_source);
}
