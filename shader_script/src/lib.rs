use std::{
    fs::OpenOptions,
    io::Read,
    path::{Path, PathBuf},
};

use serde::Deserialize;
use tracing::{debug, warn};

#[derive(Debug, Deserialize)]
pub struct ShaderMetadata {
    pub name: Option<String>,
    pub slug: Option<String>,
    pub collection: Option<String>,
    pub date: Option<chrono::NaiveDate>,
}

pub struct ShaderScript {
    pub processed_source: String,
    pub metadata: Option<ShaderMetadata>,
    pub loaded_as: PathBuf,
}

impl ShaderScript {
    pub fn get_shaders_root() -> PathBuf {
        let mut root: PathBuf = env!("CARGO_MANIFEST_DIR").into();
        root.push("../shaders");
        root
    }

    pub fn load(root: &Path, entry_path: &Path) -> Self {
        let raw_source_path = root.join(entry_path);
        debug!(?raw_source_path, "Opening shader");
        let mut raw_source_file = OpenOptions::new()
            .read(true)
            .open(&raw_source_path)
            .expect("failed to open raw shader source");
        let mut raw_source = String::new();

        raw_source_file
            .read_to_string(&mut raw_source)
            .expect("failed to load shader source");

        let mut in_header = true;
        let mut raw_header = String::new();
        let processed_source = raw_source.lines().fold(String::new(), |mut acc, line| {
            // Metadata
            if in_header && line.starts_with("///") {
                let (_, raw_metadata_line) = line.split_at(4);
                raw_header.push_str(raw_metadata_line);
                raw_header.push('\n');
            } else {
                in_header = false;
            }
            // Module system
            if line.starts_with("#include ") {
                let dependency_name = line.split_once(' ').map(|x| x.1);
                if let Some(dependency_name) = dependency_name {
                    acc.push_str(&Self::load(root, Path::new(dependency_name)).processed_source);
                } else {
                    warn!(line, "started with #include but no dep?");
                    acc.push_str(line);
                }
            } else {
                acc.push_str(line);
            }
            acc.push('\n');
            acc
        });

        let metadata = if raw_header.is_empty() {
            None
        } else {
            Some(serde_yaml::from_str(&raw_header).expect("couldn't parse shader header"))
        };
        debug!(?metadata, "Shader header");

        Self {
            processed_source,
            metadata,
            loaded_as: entry_path.to_path_buf(),
        }
    }

    pub fn get_save_dir(&self) -> PathBuf {
        self.metadata.as_ref().map_or_else(
            || PathBuf::from("output/unknown"),
            |shader_metadata| {
                let mut save_path = PathBuf::from("output");
                if let Some(collection) = shader_metadata.collection.clone() {
                    save_path.push(collection);
                } else {
                    save_path.push("unknown");
                }
                save_path
            },
        )
    }

    pub fn get_save_filename(&self) -> PathBuf {
        self.metadata
            .as_ref()
            .and_then(|m| m.slug.as_ref())
            .map_or_else(
                || {
                    PathBuf::from(
                        self.loaded_as
                            .file_stem()
                            .expect("no shader file stem or slug metadata?"),
                    )
                },
                PathBuf::from,
            )
    }
}

pub fn _get_shaders_in_dir(dir: &Path) -> std::io::Result<Vec<PathBuf>> {
    let mut result = vec![];
    for entry in std::fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_file()
            && path
                .extension()
                .and_then(std::ffi::OsStr::to_str)
                .map_or(false, |file| file.starts_with("glsl"))
        {
            result.push(path);
        }
    }
    Ok(result)
}
