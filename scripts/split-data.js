const {readFileSync, createWriteStream} = require('fs');

const dataGen = readFileSync('data-gen.json', 'utf8');

const data = dataGen.split('\n').filter(val => val.length > 1).map(val => {
    return JSON.parse(val)
});

const files = {
    4: createWriteStream('data/order-4.json', {flags: 'a'}),
    5: createWriteStream('data/order-5.json', {flags: 'a'}),
    6: createWriteStream('data/order-6.json', {flags: 'a'}),
    7: createWriteStream('data/order-7.json', {flags: 'a'}),
    8: createWriteStream('data/order-8.json', {flags: 'a'}),
}

for (const datum of data) {
    const order = Math.floor(Math.log10(datum.iterations)) + 1;
    files[order].write(`${JSON.stringify(datum)}\n`);
}
// console.log(data);
console.log(data.length);