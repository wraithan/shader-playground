#!/bin/bash

shaders=./shaders/*
cargo build --release
for f in $shaders; do
    if [ -f "$f" ]; then
        echo Testing: $(basename "$f") 
        target/release/shader-playground render --shader $(basename "$f") --fps 1 -r 100 --mode once --png-batch --width 2560 --height 1600 --flywheel
    fi
done