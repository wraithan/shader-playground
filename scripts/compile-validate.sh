#!/bin/bash

shaders=./shaders/*
cargo build --release
for f in $shaders; do
    if [ -f "$f" ]; then
        echo Testing: $(basename "$f") 
        target/release/shader-playground compile --shader $(basename "$f") | glslangValidator --stdin -S frag
    fi
done
