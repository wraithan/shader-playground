/// name: Mandelbrot 5
/// slug: mandlebrot_005
/// collection: mandelbrot
/// date: 2019-03-05

#version 400 core

#define MyVec2 dvec2

#include lib/my_math.glsl

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    uv *= pow(2.0, -(100.0 / 5.0)) * 2.0;

    uv.x += -0.911297;
    uv.y += 0.264504;

    MyVec2 z = MyVec2(0.0);
    int lastStep = 0;
    int limit = int(floor(u.time_s * 10.0));
    for (int i = 0; i < limit; i++) {
        MyVec2 nz = MyVec2(
            (z.x * z.x) - (z.y * z.y) + uv.x,
            (z.x * z.y) * 2.0 + uv.y
        );

        if (z.x * z.y < 4.0) {
            lastStep = i;
        } else {
            break;
        }
        z = nz;
    }
    vec3 col = vec3(0.0);

    if (lastStep == limit -1) {
        col.rgb = vec3(0.5, 0.1, 0.8);
    } else {
        col.b = 1.0 - abs(float(mod(lastStep, 32)) / 16);
    }

    Target0 = vec4(col, 1.0);
}
