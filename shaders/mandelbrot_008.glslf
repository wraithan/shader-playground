/// name: Mandelbrot 8
/// slug: mandlebrot_008
/// collection: mandelbrot
/// date: 2019-05-11

#version 400 core

#include lib/my_math.glsl
#include lib/hsvrgb.glsl

#define MyVec2 dvec2

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    uv *= pow(2.0, -(u.time_s / 5.0));

    uv.x += -0.93;
    uv.y += -0.3;

    MyVec2 z = MyVec2(0.0, 0.0);

    int lastStep = 0;
    int limit = 2000;

    double q = (uv.x - 0.25) * (uv.x - 0.25) + (uv.y * uv.y);
    bool inMainCardioid = (q * (q + (uv.x - 0.25)) < 0.25 * (uv.y * uv.y));
    bool inSecondBulb = ((uv.x + 1.0) * (uv.x + 1.0)) + (uv.y * uv.y) < (1.0 / 16.0);
    if (inMainCardioid || inSecondBulb) {
        lastStep = limit - 1;
    } else {
        for (int i = 0; i < limit; i++) {
            MyVec2 nz = MyVec2(
                (z.x * z.x) - (z.y * z.y) + uv.x,
                (z.x * z.y) * 2.0 + uv.y
            );
            if (z.x * z.y < 4.0) {
                lastStep = i;
            } else {
                break;
            }

            z = nz;
        }
    }

    float colors = 32.0;
    float value = mix(1.0/6.0, 3.1/9.0, abs((mod(lastStep, colors * 2.0) / colors) - 1.0));

    Target0 = vec4(hsv2rgb(vec3(value, 0.9, 1.0 - (value * 1.5))), 1.0);
}
