#version 150 core
in vec3 in_pos;

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

void main() {
    float top = 1.0;
    float bottom = -1.0;
    float left = -1.0;
    float right = 1.0;
    float far = 100.0;
    float near = 0.1;

    mat4 projection = mat4(
        vec4(            2.0 / (right - left),                              0.0,                          0.0, 0.0),
        vec4(                             0.0,             2.0 / (top - bottom),                          0.0, 0.0),
        vec4(                             0.0,                              0.0,          -2.0 / (far - near), 0.0),
        vec4(-(right + left) / (right - left), -(top + bottom) / (top - bottom), -(far + near) / (far - near), 1.0)
    );
    // mat4 translate = mat4(
    //     vec4(0.0),
    //     vec4(0.0),
    //     vec4(0.0, 0.0, 0.0, -5.0),
    //     vec4(0.0, 0.0, 0.0, 1.0)
    // );
    mat4 translate = mat4(1.0);
    translate[3][2] = -1.1;
    // float angle_rad = 3.14159 / 4.0;
    float angle_rad = u.time_s * 0.8;
    float cos_angle = cos(angle_rad);
    float sin_angle = sin(angle_rad);
    // clockwise
    mat4 x_rot = mat4(
        mat3(
            vec3(1.0,       0.0,        0.0),
            vec3(0.0, cos_angle, -sin_angle),
            vec3(0.0, sin_angle,  cos_angle)
        )
    );
    // clockwise
    mat4 y_rot = mat4(
        mat3(
            vec3(cos_angle, 0.0, -sin_angle),
            vec3(      0.0, 1.0,        0.0),
            vec3(sin_angle, 0.0,  cos_angle)
        )
    );
    // counter-clockwise
    // mat4 z_rot = mat4(
    //     mat3(
    //         vec3(-cos_angle,  sin_angle, 0.0),
    //         vec3(-sin_angle, -cos_angle, 0.0),
    //         vec3(       0.0,        0.0, 1.0)
    //     )
    // );
    mat4 z_rot = mat4(
        mat2(
            vec2(cos_angle, sin_angle),
            vec2(-sin_angle, cos_angle)
        )
    );
    // z_rot *= -1;
    mat4 rotation = x_rot * y_rot * z_rot;
    // mat4 rotation = transpose(z_rot);
    mat4 scale = mat4(mat3(0.7));

    gl_Position = projection * translate * rotation * scale * vec4(in_pos, 1.0);
}