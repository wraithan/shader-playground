
#version 150 core
in vec2 in_pos;
in vec4 in_col;
out vec4 v_col;

void main() {
    v_col = in_col;
    gl_Position = vec4(in_pos, 0.0, 1.0);
}
        