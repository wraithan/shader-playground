/// name: Mandelbrot 6
/// slug: mandlebrot_006
/// collection: mandelbrot
/// date: 2019-03-29

#version 400 core

#define MyVec2 dvec2

#include lib/my_math.glsl

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    uv *= pow(2.0, -(100.0 / 5.0)) * 2.0;

    uv.x += -0.911297;
    uv.y += 0.264504;

    MyVec2 z = MyVec2(0.0);
    int petalStep = 0;
    int mandelStep = 0;
    int limit = int(floor(u.time_s * 10.0));
    for (int i = 0; i < limit; i++) {
        MyVec2 nz = MyVec2(
            (z.x * z.x) - (z.y * z.y) + uv.x,
            (z.x * z.y) * 2.0 + uv.y
        );

        if (z.x * z.y < 4.0) {
            petalStep = i;
        }

        if (z.x * z.x + z.y * z.y < 4.0) {
            mandelStep = i;
        }
        
        if (petalStep != i && mandelStep != i) {
            break;
        }
        z = nz;
    }
    vec3 petal = vec3(0.0);
    vec3 mandel = vec3(0.0);
    // vec3 insideMandel = vec3(0.62, 0.32, 0.17); brown
    // vec3 insideMandel = vec3(0.1, 0.2, 0.3);
    vec3 insideMandel = vec3(0.0);

    if (petalStep == limit - 1) {
        petal.rgb = insideMandel;
    } else {
        petal.b = 1.0 - abs(float(mod(petalStep, 32)) / 16);
    }

    if (mandelStep == limit - 1) {
        mandel.rgb = insideMandel;
    } else {
        mandel.g = mix(0.0, 1.0 - abs(float(mod(mandelStep, 32)) / 16), 0.8);
    }


    Target0 = vec4(mix(mandel, petal, 0.5), 1.0);
}
