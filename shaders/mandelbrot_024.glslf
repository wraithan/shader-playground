/// name: Mandelbrot 24
/// slug: mandlebrot_024
/// collection: mandelbrot
/// date: 2019-05-25

#version 400 core

#include lib/my_math.glsl
#include lib/hsvrgb.glsl

#define MyVec2 dvec2
#define MyMat2 dmat2

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

vec2 computeScreenUV (vec2 fragCoord, vec2 resolution) {
    vec2 uv = (fragCoord / resolution) - 0.5;
    float lowerRes = min(resolution.x, resolution.y);
    if (resolution.x == lowerRes) {
        uv.y *= resolution.y / resolution.x;
    } else {
        uv.x *= resolution.x / resolution.y;
    }
    return uv;
}

MyVec2 computeUV (vec2 screenUv) {
    float zoomTime = u.time_s;
    MyVec2 uv = MyVec2(screenUv);
    uv *= pow(2.0, -(zoomTime / 5.0));

    uv.x += -1.18720015lf;
    uv.y += -0.304999280002lf;
    return uv;
}

MyMat2 computeTexel(MyVec2 uv, float colors) {
    MyVec2 z = MyVec2(0.0, 0.0);

    int lastStep = 0;
    int limit = max(int(u.time_s * 4.0), 50);
    MyVec2 nz = MyVec2(0.0, 0.0);

    float cutoff = (u.time_s / 20.0) + 2.0;

    for (int i = 0; i < limit; i++) {
        nz = MyVec2(
            (z.x * z.x) - (z.y * z.y) + uv.x,
            (z.x * z.y) * 2.0 + uv.y
        );

        if (abs(nz.x) < cutoff && abs(nz.y) < cutoff) {
            lastStep = i;
        } else {
            break;
        }

        z = nz;
    }

    return MyMat2(z, nz);
}

void main() {
    vec2 screenUV = computeScreenUV(gl_FragCoord.xy, u.resolution);
    vec2 texelSize = 1.0 / u.resolution;
    if ((screenUV.x > -texelSize.x && screenUV.x < texelSize.x) || (screenUV.y > -texelSize.y && screenUV.y < texelSize.y)) {
        Target0 = vec4(1.0);
        return;
    }
    float colorTime = (u.time_s + 10.0) / 10.0;
    float cLimit = 30.0;
    float colors = abs((mod(u.time_s, cLimit) / (cLimit / 2.0)) - (cLimit / 2.0));

    // float vLimit = (mod(u.time_s, 20.0) + 10.0);
    float vLimit = 0.4;
    float vMin = 0.3;

    if (screenUV.x < 0.0 && screenUV.y > 0.0) {
        // tl
        screenUV.x = remix(screenUV.x, -0.5, 0.0, -0.5, 0.5);
        screenUV.y = remix(screenUV.y, 0.0, 0.5, -0.5, 0.5);
        MyVec2 mandelUV = computeUV(screenUV);
        MyMat2 result = computeTexel(mandelUV, colors);
        float value = abs((mod(abs(dot(vec2(mandelUV), vec2(result[1]))) + colorTime, colors * 2.0) / colors) - 1.0);
        // float value = mod(rawValue, 1.0) + colorTime;
        Target0 = vec4(hsv2rgb(vec3(value, 1.0, remix(mod(value, vLimit), 0.0, vLimit, vMin, 0.6))), 1.0);
    } else if (screenUV.x > 0.0 && screenUV.y > 0.0) {
        // tr
        screenUV.x = remix(screenUV.x, 0.0, 0.5, -0.5, 0.5);
        screenUV.y = remix(screenUV.y, 0.0, 0.5, -0.5, 0.5);
        MyVec2 mandelUV = computeUV(screenUV);
        MyMat2 result = computeTexel(mandelUV, colors);
        float value = abs((mod(abs(dot(vec2(mandelUV), vec2(result[1]))) + colorTime, colors * 2.0) / colors) - 1.0) + 0.25;
        // float value = mod(rawValue, 1.0) + colorTime;
        Target0 = vec4(hsv2rgb(vec3(value, 1.0, remix(mod(value, vLimit), 0.0, vLimit, vMin, 0.6))), 1.0);
    } else if (screenUV.x < 0.0 && screenUV.y < 0.0) {
        // bl
        screenUV.x = remix(screenUV.x, -0.5, 0.0, -0.5, 0.5);
        screenUV.y = remix(screenUV.y, -0.5, 0.0, -0.5, 0.5);
        MyVec2 mandelUV = computeUV(screenUV);
        MyMat2 result = computeTexel(mandelUV, colors);
        float value = abs((mod(abs(dot(vec2(mandelUV), vec2(result[1]))) + colorTime, colors * 2.0) / colors) - 1.0) + 0.5;
        // float value = mod(rawValue, 1.0) + colorTime;
        Target0 = vec4(hsv2rgb(vec3(value, 1.0, remix(mod(value, vLimit), 0.0, vLimit, vMin, 0.6))), 1.0);
    } else if (screenUV.x > 0.0 && screenUV.y < 0.0) {
        // br
        screenUV.x = remix(screenUV.x, 0.0, 0.5, -0.5, 0.5);
        screenUV.y = remix(screenUV.y, -0.5, 0.0, -0.5, 0.5);
        MyVec2 mandelUV = computeUV(screenUV);
        MyMat2 result = computeTexel(mandelUV, colors);
        float value = abs((mod(abs(dot(vec2(mandelUV), vec2(result[1]))) + colorTime, colors * 2.0) / colors) - 1.0) + 0.75;
        // float value = mod(rawValue, 1.0) + colorTime;
        Target0 = vec4(hsv2rgb(vec3(value, 1.0, remix(mod(value, vLimit), 0.0, vLimit, vMin, 0.6))), 1.0);
    }
}
