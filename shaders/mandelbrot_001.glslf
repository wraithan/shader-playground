/// name: Mandelbrot 1
/// slug: mandlebrot_001
/// collection: mandelbrot
/// date: 2019-02-23

#version 400 core

#define MyVec2 dvec2

#include lib/my_math.glsl

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    uv *= pow(2.0, -(u.time_s / 5.0));

    // Center on ~1/4th on the real number line
    uv.x += 0.259;
    uv.y += 0.0015;

    MyVec2 z = MyVec2(0.0, 0.0);

    int lastStep = 0;
    int limit = 2000;
    for (int i = 0; i < limit; i++) {
        MyVec2 nz = MyVec2(
            (z.x * z.x) - (z.y * z.y) + uv.x,
            (z.x * z.y) * 2.0 + uv.y
        );
        if (z.x < 2.0) {
            lastStep = i;
        } else {
            break;
        }

        z = nz;
    }

    vec3 col = vec3(0.0);

    if (lastStep == limit - 1) {
        col.rg = abs(vec2(z));
    }

    Target0 = vec4(col, 1.0);
}
