/// name: Mandelbrot 23
/// slug: mandlebrot_023
/// collection: mandelbrot
/// date: 2019-05-20

#version 400 core

#include lib/my_math.glsl
#include lib/hsvrgb.glsl

#define MyVec2 dvec2

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

MyVec2 computeScreenUV (vec2 fragCoord) {
    MyVec2 uv = MyVec2(fragCoord/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    return uv;
}

MyVec2 computeUV (vec2 fragCoord) {
    MyVec2 uv = computeScreenUV(fragCoord);
    float zoomTime = u.time_s;
    uv *= pow(2.0, -(zoomTime / 5.0));

    uv.x += -1.18720015lf;
    uv.y += -0.304999280002lf;
    return uv;
}

float computeTexel(MyVec2 uv) {
    MyVec2 z = MyVec2(0.0, 0.0);

    float colors = 48;
    int lastStep = 0;
    int limit = int((colors * 10.0) + colors / 2.0);
    MyVec2 nz = MyVec2(0.0, 0.0);

    float cutoff = (u.time_s / 50.0) + 2.0;

    for (int i = 0; i < limit; i++) {
        nz = MyVec2(
            (z.x * z.x) - (z.y * z.y) + uv.x,
            (z.x * z.y) * 2.0 + uv.y
        );
        
        if (abs(nz.x) < cutoff && abs(nz.y) < cutoff) {
            lastStep = i;
        } else {
            break;
        }

        z = nz;
    }

    return abs((mod(abs(dot(vec2(z), vec2(nz))), colors * 2.0) / colors) - 1.0);
}

void main() {
    MyVec2 screenUV = computeScreenUV(gl_FragCoord.xy);
    vec2 texelSize = 1.0 / u.resolution;
    if ((screenUV.x > -texelSize.x && screenUV.x < texelSize.x) || (screenUV.y > -texelSize.y && screenUV.y < texelSize.y)) {
        Target0 = vec4(1.0);
        return;
    }
    float count = 0.0;
    float total = 0.0;

    if (screenUV.y < 0.0) {
        vec2 subTexelSize = vec2(1.0 / (u.resolution.x * 2.0), 1.0 / (u.resolution.y * 2.0));
        MyVec2 uv1 = computeUV(gl_FragCoord.xy - subTexelSize);
        float value1 = computeTexel(uv1);
        MyVec2 uv2 = computeUV(vec2(gl_FragCoord.xy - vec2(subTexelSize.x, -subTexelSize.y)));
        float value2 = computeTexel(uv2);
        MyVec2 uv3 = computeUV(vec2(gl_FragCoord.xy - vec2(-subTexelSize.x, -subTexelSize.y)));
        float value3 = computeTexel(uv3);
        MyVec2 uv4 = computeUV(vec2(gl_FragCoord.xy - vec2(-subTexelSize.x, subTexelSize.y)));
        float value4 = computeTexel(uv4);

        count = 4.0;
        total = value1 + value2 + value3 + value4;

        if (screenUV.x > 0.0) {
            total += computeTexel(computeUV(gl_FragCoord.xy)) * 4.0;
            count += 4.0;
        }
    } else {
        total = computeTexel(computeUV(gl_FragCoord.xy)) * 4.0;
        count = 4.0;
    }

    float colorTime = 1.67;
    float value = (total / count) + colorTime;
    Target0 = vec4(hsv2rgb(vec3(value, 1.0, 0.5)), 1.0);
}
