#define GOLDEN_RATIO 1.61803398875
#define PI 3.14159265359

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}

float unmix(float startLow, float startHigh, float val) {
    return (val - startLow) / (startHigh - startLow);
}

vec2 unmix(vec2 startLow, vec2 startHigh, float val) {
    return (val - startLow) / (startHigh - startLow);
}

vec3 unmix(vec3 startLow, vec3 startHigh, float val) {
    return (val - startLow) / (startHigh - startLow);
}

vec4 unmix(vec4 startLow, vec4 startHigh, float val) {
    return (val - startLow) / (startHigh - startLow);
}

float remix(
    float val,
    float startLow, float startHigh,
    float endLow, float endHigh) {
    return mix(endLow, endHigh, unmix(startLow, startHigh, val));
}
