/// name: Mandelbrot 22
/// slug: mandlebrot_022
/// collection: mandelbrot
/// date: 2019-05-18

#version 400 core

#include lib/my_math.glsl
#include lib/hsvrgb.glsl

#define MyVec2 dvec2

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    // float zoomTime = u.time_s;
    float zoomTime = 100.0;
    uv *= pow(2.0, -(zoomTime / 5.0));
    // uv *= 3.0;

    uv.x += -1.18720015lf;
    // uv.x -= 0.3;
    uv.y += -0.304999280002lf;

    MyVec2 z = MyVec2(0.0, 0.0);

    float colors = 48;
    int lastStep = 0;
    int limit = int((colors * 10.0) + colors / 2.0);
    MyVec2 nz = MyVec2(0.0, 0.0);

    float cutoff = max(u.time_s / 50.0, 1.0);
    // float cutoff = 4.0;

    double q = (uv.x - 0.25) * (uv.x - 0.25) + (uv.y * uv.y);
    bool inMainCardioid = (q * (q + (uv.x - 0.25)) < 0.25 * (uv.y * uv.y));
    bool inSecondBulb = ((uv.x + 1.0) * (uv.x + 1.0)) + (uv.y * uv.y) < (1.0 / 16.0);
    inMainCardioid = false;
    inSecondBulb = false;
    if (inMainCardioid || inSecondBulb) {
        lastStep = limit - 1;
    } else {
        for (int i = 0; i < limit; i++) {
            nz = MyVec2(
                (z.x * z.x) - (z.y * z.y) + uv.x,
                (z.x * z.y) * 2.0 + uv.y
            );
            // if (abs(z.x) < cutoff || abs(z.y) < cutoff) {
            // if (nz.x * nz.x + nz.y * nz.y < cutoff) {
            // if (abs(z.x * z.y) < cutoff) {
            if (abs(nz.x) < cutoff || abs(nz.y) < cutoff) {
                lastStep = i;
            } else {
                break;
            }

            z = nz;
        }
    }

    float colorTime = u.time_s;
    // float colorTime = 12.3;
    // float colorTime = 14.5;
    // float colorTime = 8.3;
    // float colorMin = mod(colorTime / 10.0, 1.0);
    // float colorMax = colorMin + 0.15;

    // float value = mix(colorMin, colorMax, abs((mod(lastStep, colors * 2.0) / colors) - 1.0));

    // Target0 = vec4(hsv2rgb(vec3(value, 1.0, ((max(colorMin, colorMax) + 0.01) * 2.0) - (value * 2.0))), 1.0);

    float value = mod(abs(dot(vec2(z), vec2(nz))), colors) / colors + colorTime;
    Target0 = vec4(hsv2rgb(vec3(value, 1.0, 0.5)), 1.0);
}
