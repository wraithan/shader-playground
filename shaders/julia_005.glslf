/// name: Filled Julia 5
/// slug: julia_005
/// collection: julia
/// date: 2019-03-16

#version 400 core

#define MyVec2 vec2

#include lib/my_math.glsl
#include lib/hsvrgb.glsl

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    uv *= 2.0;

    MyVec2 c = MyVec2(pow(2, -GOLDEN_RATIO/1.6), 1/PI);


    MyVec2 z = (MyVec2(uv) + MyVec2(1.8, 4.7)) * pow(2, -u.time_s / 10);

    int lastStep = 0;
    int limit = 2000;
    MyVec2 biggest = MyVec2(0.0, 0.0);
    for (int i = 0; i < limit; i++) {
        MyVec2 nz = MyVec2(
            (z.x * z.x) - (z.y * z.y) + c.x,
            (z.x * z.y) * 2.0 + c.y
        );

        if (z.x * z.y  < 4.0) {
            lastStep = i;
        } else {
            break;
        }
        z = nz;
    }
    vec3 col = vec3(0.0);

    // if (lastStep == limit - 1) {
    //     col.rgb = vec3(0.3, 0.5, 0.9);
    // } else {
    //     col.b = 1.0 - abs(float(mod(lastStep, 32)) / 16);
    // }
    vec3 values = abs((mod(vec3(float(lastStep), z.x, z.y), 32.0) / 16.0) - 1.0);

    Target0 = vec4(hsv2rgb(vec3(values.x, mix(0.6, 1.0, -z.x), 0.5)), 1.0);
}
