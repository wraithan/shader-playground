/// name: Shader Toy Experiment 4
/// slug: shader_exp_4
/// collection: experiments
/// date: 2019-02-17

#version 150 core

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

#include lib/classicnoise3d.glsl
#include lib/my_math.glsl

// https://github.com/ronja-tutorials/ShaderTutorials/blob/master/Assets/027_Layered_Noise/layered_perlin_noise_3d.shader
float layerNoise(vec3 index) {
    float result = 0.0;
    float frequency = 1.0;
    float factor = 1.0;
    float persistence = 0.3;
    float roughness = 3.5;
    int octaves = 4;
    
    for (int i = 0; i < octaves; i++) { 
        result = result + cnoise(index * frequency + float(i) * 0.72345) * factor;
        factor *= persistence;
        frequency *= roughness;
    }
    
    return result;
}

void main() {
    // build up noise value
    vec2 uv = gl_FragCoord.xy/u.resolution;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    float t = u.time_s / 2.0;
    vec2 noiseIndex = uv * rotate2d(PI) * 10.0;;
    float noiseVal = 0.0;
    
    if (uv.x < 0.5) {
		// original put any replaced functionality in here
        noiseVal = cnoise(vec3(noiseIndex, t));
    } else {
        // new noise
        noiseVal = layerNoise(vec3(noiseIndex, t));
    }
    
    noiseVal = remix(noiseVal, -1.0, 1.0, 0.0, 1.0);
    
    // Interpret noise into colors
    vec3 col = vec3(0.0);
    
    vec2 blueNoiseRange = vec2(0.5, 0.6);
    vec2 blueColorRange = vec2(0.4, 0.8);
    float blueMiddle = (blueNoiseRange.x + blueNoiseRange.y) / 2.0;
    if (noiseVal >= blueNoiseRange.x && noiseVal < blueMiddle) {
        col.b = remix(
            noiseVal,
            blueNoiseRange.x, blueMiddle,
            blueColorRange.x, blueColorRange.y
        );
    } else if (noiseVal >= blueMiddle && noiseVal < blueNoiseRange.y) {
        col.b = remix(
            noiseVal,
            blueMiddle, blueNoiseRange.y,
            blueColorRange.y, blueColorRange.x
        );
    }

    vec2 greenNoiseRange = vec2(0.515, 0.56);
    vec2 greenColorRange = vec2(0.0, 0.5);
    float greenMiddle = (greenNoiseRange.x + greenNoiseRange.y) / 2.0;
    if (noiseVal >= greenNoiseRange.x && noiseVal < greenMiddle) {
        float mappedColor = remix(
            noiseVal,
            greenNoiseRange.x, greenMiddle,
            greenColorRange.x, greenColorRange.y
        );
        col.r = mappedColor;
        if (mappedColor >= 0.25) {
            col.g = mappedColor;
        } else {         
        	col.g = mappedColor * 0.75;
        }
    } else if (noiseVal >= greenMiddle && noiseVal < greenNoiseRange.y) {
        float mappedColor = remix(
            noiseVal,
            greenMiddle, greenNoiseRange.y,
            greenColorRange.y, greenColorRange.x
        );
        col.r = mappedColor;
        if (mappedColor >= 0.25) {
            col.g = mappedColor;
        } else {         
        	col.g = mappedColor * 0.75;
        }
    }
    
    float lineWidth = 0.001;
    if (uv.x > (0.5 - lineWidth) && uv.x < (0.5 + lineWidth)) {
        col = vec3(1.0);
    }

    // Output to screen
    Target0 = vec4(col, 1.0);
}
