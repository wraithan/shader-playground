/// name: Texture Test 1
/// slug: texture_test_001
/// collection: experiments
/// date: 2019-03-29

#version 400 core

#define MyVec2 vec2

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

uniform sampler2D Tex1;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution);// - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    // vec2 uv = (gl_FragCoord.xy/u.resolution);
    // uv.y = 1.0 - uv.y;

    Target0 = texture(Tex1, uv);
}
