/// name: Filled Julia 3
/// slug: julia_003
/// collection: julia
/// date: 2019-03-10

#version 400 core

#define MyVec2 vec2

#include lib/my_math.glsl

uniform Uniforms {
    vec2 resolution;
    float time_s;
} u;

out vec4 Target0;

void main() {
    MyVec2 uv = MyVec2(gl_FragCoord.xy/u.resolution) - 0.5;
    float lowerRes = min(u.resolution.x, u.resolution.y);
    if (u.resolution.x == lowerRes) {
        uv.y *= u.resolution.y / u.resolution.x;
    } else {
        uv.x *= u.resolution.x / u.resolution.y;
    }
    uv *= 2.0;

    MyVec2 c = MyVec2(1/PI, u.time_s / 100);

    MyVec2 z = MyVec2(uv);
    int lastStep = 0;
    int limit = 2000;
    MyVec2 biggest = MyVec2(0.0, 0.0);
    for (int i = 0; i < limit; i++) {
        MyVec2 nz = MyVec2(
            (z.x * z.x) - (z.y * z.y) + c.x,
            (z.x * z.y) * 2.0 + c.y
        );

        if (z.x * z.y  < 4.0) {
            lastStep = i;
        } else {
            break;
        }
        z = nz;
    }
    vec3 col = vec3(0.0);

    if (lastStep == limit - 1) {
        col.rgb = vec3(0.3, 0.5, 0.9);
    } else {
        col.b = 1.0 - abs(float(mod(lastStep, 32)) / 16);
    }

    Target0 = vec4(col, 1.0);
}
