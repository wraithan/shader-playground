/// name: Solid Color Blue
/// slug: solid_color_blue
/// collection: utilities
/// date: 2019-04-11

#version 150 core

in vec4 v_col;
out vec4 Target0;

void main() {
    // Target0 = vec4(0.0, 0.0, 1.0, 1.0);
    Target0 = v_col;
}
